//
//  introViewController.swift
//  Recall Check
//
//  Created by iBuildx-Mac3 on 5/3/17.
//  Copyright © 2017 iBuildX. All rights reserved.
//

import Foundation
import EAIntroView



class introViewController: UIViewController  ,EAIntroDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        createAndShowTheIntroView()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.performSegue(withIdentifier: "backToScannerVC", sender: nil)
    }
    
    func createAndShowTheIntroView() {
        
        menuView.isHidden = true
        
        let frameView : UIView = UIView(frame: CGRect(x: 20, y: self.view.frame.size.width/2, width:self.view.frame.size.width/2 , height: self.view.frame.size.height/1.5))
        
        let page1 = EAIntroPage()
        page1.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page1.titleIconView = UIImageView(image: UIImage(named: "imgCamera")!)
        page1.titleIconView.contentMode = UIViewContentMode.scaleAspectFit
        page1.titleIconView.clipsToBounds = true
        page1.titleIconView.frame = frameView.frame
        page1.title = "Vin Scan Page"
        page1.desc = ""
        
        let page2 = EAIntroPage()
        page2.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page2.titleIconView = UIImageView(image: UIImage(named: "imgRecallCheck-1")!)
        page2.titleIconView.contentMode = UIViewContentMode.scaleAspectFit
        page2.titleIconView.clipsToBounds = true
        page2.titleIconView.frame = frameView.frame
        page2.title = "You can manually scan a VIN"
        page2.desc = ""
        
        let page3 = EAIntroPage()
        page3.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page3.titleIconView = UIImageView(image: UIImage(named: "imgHistory-1")!)
        page3.titleIconView.contentMode = UIViewContentMode.scaleAspectFit
        page3.titleIconView.clipsToBounds = true
        page3.titleIconView.frame = frameView.frame
        page3.title = "History Screen"
        page3.desc = ""
        
        let page4 = EAIntroPage()
        page4.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page4.titleIconView = UIImageView(image: UIImage(named: "alertScreen-1")!)
        page4.titleIconView.contentMode = UIViewContentMode.scaleAspectFit
        page4.titleIconView.clipsToBounds = true
        page4.titleIconView.frame = frameView.frame
        page4.title = ""
        page4.desc = ""
        
        
        let page8 = EAIntroPage()
        page8.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page8.titleIconView = UIImageView(image: UIImage(named: "cellSwipeScreen-1")!)
        page8.titleIconView.contentMode = UIViewContentMode.scaleAspectFit
        page8.titleIconView.clipsToBounds = true
        page8.titleIconView.frame = frameView.frame
        page8.title = ""
        page8.desc = ""
        
        let intro = EAIntroView(frame: self.view.bounds, andPages: [page1, page2 , page3 , page4 , page8])
        intro?.delegate = self
        intro?.show(in: self.view, animateDuration: 0.0)
    }
    
    
    func introDidFinish(_ introView: EAIntroView!, wasSkipped: Bool) {
        self.performSegue(withIdentifier: "backToScannerVC", sender: nil)
    }
    
    func introWillFinish(_ introView: EAIntroView!, wasSkipped: Bool) {
        
    }
    
    
}
