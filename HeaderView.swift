import UIKit

protocol HeaderViewDelegate: class {
  func headerViewDidPressClose(_ headerView: HeaderView)
}

/**
 Header view that simulates a navigation bar.
 */
class HeaderView: UIView {

    /// Title label.
    lazy var label: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.text = Title.text
        label.font = Title.font
        label.textColor = Title.color
        label.numberOfLines = 1
        label.textAlignment = .left
        
        return label
    }()
    
    /// Close button.
    lazy var button: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle(CloseButton.text, for: .normal)
        button.titleLabel?.font = CloseButton.font
        button.tintColor = CloseButton.color
        button.addTarget(self, action: #selector(buttonDidPress), for: .touchUpInside)
        
        return button
    }()
    
    /// Header view delegate.
    weak var delegate: HeaderViewDelegate?
    
    // MARK: - Initialization
    
    /**
     Creates a new instance of `HeaderView`.
     
     - Parameter frame: View frame.
     */
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(red: 0.0 / 255.0, green: 84.0 / 255.0, blue: 161.0 / 255.0, alpha: 1)
        
        [label, button].forEach {
            addSubview($0)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Layout
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let padding: CGFloat = 0
        let labelHeight: CGFloat = 40
        
        //button.sizeToFit()
        
        button.frame.size.width = 45;
        button.frame.size.height = 45;
        
        //button.frame.origin = CGPoint(x: frame.width - 40 , y: ((frame.height - button.frame.height) / 2) + padding)
        
        //button.frame.origin = CGPoint(x: frame.width - 40 , y: ((frame.height - button.frame.height) / 2) + padding)
        button.frame.origin = CGPoint(x: frame.width - 40 , y: 10)
        
        print("Success with JSON: \(button.frame)")
        
        label.frame = CGRect(
            x: 10, y: 5,
            width: frame.width, height: labelHeight)
    }

  // MARK: - Actions

  /**
   Close button action handler.
   */
  func buttonDidPress() {
    delegate?.headerViewDidPressClose(self)
  }
}
