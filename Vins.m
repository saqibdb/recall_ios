//
//  Vins.m
//  RecallMasters VIN Check
//
//  Created by ibuildx on 10/10/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vins.h"



@implementation Vins
@synthesize VinId, vehicleName , VinNo ,recallCount , time;

//-(id) initWithName:(NSString*)_name andVinNo:(NSString*)_vinNo andRecallCounts:(int)_recallCount andTime:(NSString*)_time {
//    if ((self = [super init])) {
//        self.VinId = _VinId;
//        self.vehicleName = _vehicleName;
//        self.VinId = _VinId ;
//        self.VinNo = _VinNo ;
//        
//    }
//    return self;
//}
-(id) initWithId:(int)_VinId andName:(NSString*)_vehicleName andVinNo:(NSString*)_VinNo andRecallCount:(int)_recallCount andTime:(NSString*)_time {
    if ((self = [super init])) {
        self.VinId = _VinId;
        self.vehicleName = _vehicleName;
        self.recallCount = _recallCount ;
        self.VinNo = _VinNo ;
        self.time = _time ;

    }
    return self;
}


+(Vins*) eventFromStatement:(sqlite3_stmt*) statement {
    int vinId = sqlite3_column_int(statement, 0);
     int recallcount = sqlite3_column_int(statement, 1);
    NSString* vehicleName= [NSString stringWithUTF8String:(char*) sqlite3_column_text(statement, 2)];
      NSString* VinNo= [NSString stringWithUTF8String:(char*) sqlite3_column_text(statement, 3)];
      NSString* time= [NSString stringWithUTF8String:(char*) sqlite3_column_text(statement, 4)];
    Vins *vin = [[Vins alloc] initWithId:vinId andName:vehicleName andVinNo:VinNo andRecallCount:recallcount andTime:time];
    return vin;
}



+(NSArray*)getAllVins{
    __block NSMutableArray *vinsArray = [[NSMutableArray alloc] init];
    NSString *query = [NSString stringWithFormat:@"SELECT vinId,recallCount ,  vehicleName, vinNo ,time  FROM Vins"];
    [self selectDataForQuery:query withResultBlock:^int(sqlite3_stmt *statement) {
        Vins *vin;
        while (sqlite3_step(statement) == SQLITE_ROW) {
            vin = [Vins eventFromStatement:statement];
            [vinsArray addObject:vin];
        }
        return SQLITE_OK;
    }];
    return vinsArray;
}


+(Vins *)findByvinId :(int)vinId{
    __block Vins* vin;
    NSString *query = [NSString stringWithFormat:@"SELECT Route_id , Route_Name FROM Route WHERE Route_id = %i", vinId];
    [self selectDataForQuery:query withResultBlock:^int(sqlite3_stmt *statement) {
        int rc;
        if((rc=sqlite3_step(statement)) == SQLITE_ROW){
            vin = [Vins eventFromStatement:statement];
        }
        return rc;
    }];
    return vin;
}

-(BOOL) saveToDb {
    if(self.VinId > 0) {
        Vins *vin = [Vins findByvinId:self.VinId];
        
        if(vin == nil) {
            NSString* query = [NSString stringWithFormat:@"INSERT INTO Vins(vinId, VehicleName , vinNo , recallCount , time) VALUES ('%lu','%@','%@','%d','%@')", (unsigned long)self.VinId, self.vehicleName ,self.VinNo , self.recallCount ,self.time];
            return [Vins insertDataForQuery:query] == SQLITE_DONE;
        } else {
           NSString* query = [NSString stringWithFormat:@"UPDATE Vins SET vehicleName = '%@'   WHERE vinId = %i" ,  self.vehicleName , self.VinId];
           return [Vins updateDataForQuery:query] == SQLITE_DONE;
        }
    } else {
        NSString* query = [NSString stringWithFormat:@"INSERT INTO Vins( vehicleName , vinNo , recallCount , time) VALUES ('%@','%@','%d','%@')", self.vehicleName ,self.VinNo , self.recallCount ,self.time];
        int rc = [Vins insertDataForQuery:query withBlock:^(NSInteger rowid) {
           
                    }];
        
        return rc == SQLITE_DONE;
    }
}

+(void)deleteVin:(int)vinId{
     [Vins updateDataForQuery:[NSString stringWithFormat: @"DELETE FROM Vins WHERE vinId = %i",vinId]];
}

@end
