//
//  HistoryViewController.swift
//  RecallMasters VIN Check
//
//  Created by ibuildx on 9/1/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import SVProgressHUD
import EAIntroView


class HistoryViewController: UIViewController, UIGestureRecognizerDelegate,UITableViewDataSource, UITableViewDelegate ,MGSwipeTableCellDelegate  ,EAIntroDelegate{
    
    
    var popUp = AlertOptions()
    @IBOutlet weak var toolBarView: UIView!
    
    // MARK: - outlets and variables
    
    @IBOutlet weak var frameView: UIView!
    var isFrom : String = ""
    
    
    @IBOutlet weak var monitorRecallBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoHistory: UILabel!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var openPopUpBtn: UIButton!
    
    @IBOutlet var btnAlert: UIButton!
    @IBOutlet var btnRecheck: UIButton!
    
    
    var HistoryArray: NSMutableArray = NSMutableArray()
    var capturedCode : String = ""
    var vin : NSDictionary = NSDictionary()
    let cont = BarcodeScannerController()
    
    
    var allResultsDicts : [[String:Any]]!
    
    // MARK: - view did load
    override func viewDidLoad() {
        
        super.viewDidLoad()
    
        UIApplication.shared.statusBarStyle = .lightContent
        self.lblNoHistory.isHidden = true
        self.toolBarView.isHidden = true
        self.popUpView.isHidden=true
        cont.codeDelegate = self
        cont.errorDelegate = self
        cont.dismissalDelegate = self
        self.tableView.dataSource = self
        self.tableView.delegate = self
        // popview shadow
        self.popUpView.layer.cornerRadius=2.0
        
        //self.popUpView.layer.borderColor = UIColor.lightGray.cgColor
        //self.popUpView.layer.borderWidth = 3.0
        
        /*
        self.popUpView.layer.masksToBounds = true;
        self.popUpView.layer.shadowColor = UIColor.black.cgColor
        self.popUpView.layer.shadowOpacity = 0.8
        self.popUpView.layer.shadowOffset = CGSize(width: CGFloat(2.0), height: CGFloat(2.0))
        self.popUpView.layer.shadowRadius = 3.0
        self.popUpView.layer.shouldRasterize = true
        */
        
        
        
        self.popUpView.layer.masksToBounds = false
        self.popUpView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.popUpView.layer.shadowRadius = 3.0
        self.popUpView.layer.shadowOpacity = 0.7
        
        /*
        let path = UIBezierPath()
        
        // Start at the Top Left Corner
        path.move(to: CGPoint(x: 0.0, y: 0.0))
        
        // Move to the Top Right Corner
        path.addLine(to: CGPoint(x: self.popUpView.frame.size.width, y: 0.0))
        
        // Move to the Bottom Right Corner
        path.addLine(to: CGPoint(x: self.popUpView.frame.size.width, y: self.popUpView.frame.size.height))
        
        // This is the extra point in the middle :) Its the secret sauce.
        path.addLine(to: CGPoint(x: self.popUpView.frame.size.width/2.0, y: self.popUpView.frame.size.height/2.0))
        
        // Move to the Bottom Left Corner
        path.addLine(to: CGPoint(x: 0.0, y: self.popUpView.frame.size.height))
        
        path.close()
        
        self.popUpView.layer.shadowPath = path.cgPath
*/
        
        
        
        
        
        
        
        
        let StoredToken = UserDefaults.standard
        if(( StoredToken.string(forKey: "auth_token")) == "9931b14a2be3944f405a0f8f07eb739c17a00e41"){
            loginBtn.setTitle("Log in", for:.normal)
        }
            
        else {
            self.loginBtn.setTitle("Log Out", for:.normal)
        }
        
        let  auth_token = StoredToken.string(forKey: "auth_token")
        self.getVinHistoryWithUDID(requestUrl: "https://app.recallmasters.com/api/v1/lookup/history/" + auth_token! + "/")
        
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        //        self.view.addGestureRecognizer(tap)
        
        
        //self.tableView.isHidden = true
        self.lblNoHistory.isHidden = true
         //NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("handleRecallMonitoring"), object: nil)
        
        /*
        if UserDefaults.standard.bool(forKey: "isMonitoring")
        {
           
            self.monitorRecallBtn.setTitle("Monitoring Recalls", for: UIControlState.normal)
        }
        else{
             self.monitorRecallBtn.setTitle("Monitor Recalls", for: UIControlState.normal)
        }
         */
        
        
        
    }
    func methodOfReceivedNotification(notification: Notification){
        if UserDefaults.standard.bool(forKey: "isMonitoring")
        {
            self.monitorRecallBtn.setTitle("Monitoring Recalls", for: UIControlState.normal)
        }
        else{
            
            self.monitorRecallBtn.setTitle("Monitor Recalls", for: UIControlState.normal)
        }
        
    }

    @IBAction func monitorRecallAction(_ sender: UIButton) {
        
        if UserDefaults.standard.bool(forKey: "isMonitoring")
        {
            UserDefaults.standard.set(false, forKey: "isMonitoring")
            self.monitorRecallBtn.setTitle("Monitor Recalls", for: UIControlState.normal)
        }
        else{
            
            UserDefaults.standard.set(true, forKey: "isMonitoring")
            self.monitorRecallBtn.setTitle("Monitoring Recalls", for: UIControlState.normal)
        }
        NotificationCenter.default.post(name: Notification.Name("handleRecallMonitoring"), object: nil)
    }
    // MARK: - Orientation
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask{
        return .portrait
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        
        //        HistoryArray  =  NSMutableArray(array: Vins.getAllVins())
        //        print(HistoryArray)
        tableView.reloadData()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.popUpView.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - uitableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return self.HistoryArray.count
        
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let CellIdentifier = "HistoryCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath as IndexPath) as! HistoryCell
        
        let vehicle  = HistoryArray.object(at: indexPath.row) as! NSDictionary
        let vin = vehicle["vehicle"] as? NSDictionary
        
        cell.lblRecallsCount.text = "Recall counts : \(String ( vin?.value(forKey: "recall_count") as! Int))"
        cell.lblVin.text = vin?.value(forKey: "vin") as! String?
        
        let make : String = vin!.value(forKey: "make") as! String
        let modelName : String = vin!.value(forKey: "model_name") as! String
        let modelYear  =  vin?.value(forKey: "model_year")
        
        cell.lblVhecleName.text = "\(modelYear!) \(make) \(modelName)"
        
        let time = vehicle["last_time"] as? String
        cell.lblTime.text = convertDateFormater(date: time!)
        cell.lblCounts.text = String(indexPath.row + 1)
        
        
        cell.rightButtons = [MGSwipeButton(title: "Remove", icon: UIImage(named:"check.png"), backgroundColor: UIColor(
            red: 227 / 255.0,
            green: 140 / 255.0,
            blue: 148 / 255.0,
            alpha: 1
            ), callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                let vehicle  = self.HistoryArray.object(at: sender.tag) as! NSDictionary
                let itemId : String?
                itemId = "\(vehicle["id"]!)"
                
                
                
                let parameters = ["is_visible": "False"] as [String : Any]
                self.changeHistoryItem(method: .patch, parameter: parameters, vinId: itemId!)
                
                self.HistoryArray.removeObject(at: indexPath.row)
                tableView.reloadData()
                //_ =  [Vins .deleteVin(vin.vinId)]
                return true
        }),MGSwipeButton(title: "View", icon: UIImage(named:"check.png"), backgroundColor: UIColor(
            red: 25 / 255.0,
            green: 227 / 255.0,
            blue: 191 / 255.0,
            alpha: 1
            ), callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                self.capturedCode = vin?.value(forKey: "vin") as! String
                print(self.capturedCode)
                self.performSegue(withIdentifier: "historyTorecall", sender: nil)
                return true
        }),MGSwipeButton(title: "Subscribe to Alerts", icon: UIImage(named:"check.png"), backgroundColor: UIColor(
            red: 25 / 255.0,
            green: 227 / 255.0,
            blue: 191 / 255.0,
            alpha: 1
            ), callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                
                let vehicle  = self.HistoryArray.object(at: indexPath.row) as! NSDictionary
                self.vin = (vehicle["vehicle"] as? NSDictionary)!
                
                self.popUp = AlertOptions(nibName: "AlertOptions", bundle: nil)
                self.presentPopupViewController(self.popUp, animated: true, completion: {() -> Void in
                    
                    self.popUp.btnEmail.addTarget(self, action: #selector(self.alertTyperSelection) , for: UIControlEvents.touchUpInside)
                    self.popUp.btnText.addTarget(self, action: #selector(self.alertTyperSelection) , for: UIControlEvents.touchUpInside)
                    self.popUp.doneBtn.addTarget(self, action: #selector(self.alertTyperSelection) , for: UIControlEvents.touchUpInside)
                    self.popUp.btnCancel.addTarget(self, action: #selector(self.popupCancelAction) , for: UIControlEvents.touchUpInside)

                })
                
                
                return true
        }),MGSwipeButton(title: "Recheck VIN", icon: UIImage(named:"check.png"), backgroundColor: UIColor(
            red: 25 / 255.0,
            green: 227 / 255.0,
            blue: 191 / 255.0,
            alpha: 1
            ), callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                
                self.capturedCode = vin?.value(forKey: "vin") as! String
                self.performSegue(withIdentifier: "historyTorecall", sender: nil)
                return true
        })]
        
        
        return cell
    }
    func buttonClicked(_ sender:UIButton)
    {
        //for scrolling table view to selected cell
        
        
        
        let vehicle  = HistoryArray.object(at: sender.tag) as! NSDictionary
        vin = (vehicle["vehicle"] as? NSDictionary)!

       
        self.toolBarView.isHidden = false
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let vehicle  = HistoryArray.object(at: indexPath.row) as! NSDictionary
         vin = (vehicle["vehicle"] as? NSDictionary)!
 
        self.toolBarView.isHidden = false
        
        /*
        capturedCode = vin.value(forKey: "vin") as! String
        print(capturedCode)
        performSegue(withIdentifier: "historyTorecall", sender: nil)
         */
    }
    
    func convertDateFormater(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        
        guard let date = dateFormatter.date(from: date) else {
            assert(false, "no date from string")
            return ""
        }
        
        dateFormatter.dateFormat = "yyyy MMM EEEE HH:mm a"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let timeStamp = dateFormatter.string(from: date)
        
        return timeStamp
    }
    
    
    
    @IBAction func popUpEmailAction(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            self.popUp.imgEmail.image = UIImage.init(named: "radioFill")
        }
        else{
            sender.tag = 0
            self.popUp.imgEmail.image = UIImage.init(named: "radioEmpty")
        }
    }
    
    @IBAction func popUpSMSAction(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            self.popUp.imgText.image = UIImage.init(named: "radioFill")
        }
        else{
            sender.tag = 0
            self.popUp.imgText.image = UIImage.init(named: "radioEmpty")
        }
    }
    
    @IBAction func popupCancelAction(_ sender: UIButton) {
        self.dismissPopup()
    }
    
    
    
    // MARK: - toolbar View button methods
    @IBAction func alertsAction(_ sender: UIButton) {
        
        let vinNo = vin.value(forKey: "vin") as! String

        
        
        popUp = AlertOptions(nibName: "AlertOptions", bundle: nil)
        self.presentPopupViewController(popUp, animated: true, completion: {() -> Void in
            
            
            
            self.popUp.btnEmail.tag = 0
            self.popUp.imgEmail.image = UIImage.init(named: "radioEmpty")
            
            self.popUp.btnText.tag = 0
            self.popUp.imgText.image = UIImage.init(named: "radioEmpty")
            
            self.popUp.doneBtn.setTitle("SUBSCRIBE", for: .normal)

            
            
            for resultDict : [String:Any] in self.allResultsDicts!{
                let vehicleDict : [String:Any] = resultDict["vehicle"] as! [String : Any]
                let vin = vehicleDict["vin"] as! String
                print("name is \(vin)")
                print("name is \(vinNo)")
                if vin == vinNo {
                    
                    
                    let isAlertActive : Bool = resultDict["is_alert"] as! Bool
                    if isAlertActive == true {
                        let isEmailActive : Bool = resultDict["is_email"] as! Bool
                        if isEmailActive == true {
                            self.popUp.btnEmail.tag = 1
                            self.popUp.imgEmail.image = UIImage.init(named: "radioFill")
                        }
                        
                        let isSMSActive : Bool = resultDict["is_sms"] as! Bool
                        if isSMSActive == true {
                            self.popUp.btnText.tag = 1
                            self.popUp.imgText.image = UIImage.init(named: "radioFill")
                        }
                        self.popUp.doneBtn.setTitle("UNSUBSCRIBE", for: .normal)
                        
                    }

                    
                    break
                }
            }
            
            
            
            self.popUp.btnEmail.addTarget(self, action: #selector(self.popUpEmailAction) , for: UIControlEvents.touchUpInside)
            self.popUp.btnText.addTarget(self, action: #selector(self.popUpSMSAction) , for: UIControlEvents.touchUpInside)
            self.popUp.btnCancel.addTarget(self, action: #selector(self.popupCancelAction) , for: UIControlEvents.touchUpInside)
            self.popUp.doneBtn.addTarget(self, action: #selector(self.alertTyperSelection) , for: UIControlEvents.touchUpInside)

         })
        
    }
    

    
    @IBAction func alertTyperSelection(_ sender: UIButton) {
        
     
        if sender.tag == 0 {
            
            popUp.imgEmail.image = UIImage.init(named: "radioFill")
            //popUp.imgText.image = UIImage.init(named: "radioEmpty")
            
                   }
        else if sender.tag == 1
        {
            
            //popUp.imgEmail.image = UIImage.init(named: "radioEmpty")
            popUp.imgText.image = UIImage.init(named: "radioFill")
        }
            
        else
        {
            let parameters = ["vin": "\(vin.value(forKey: "vin")!)"] as [String : Any]
            print("vin  \(vin.value(forKey: "vin")!)")
            
            
            let vinNo = vin.value(forKey: "vin")! as! String
            
            for resultDict : [String:Any] in self.allResultsDicts!{
                let vehicleDict : [String:Any] = resultDict["vehicle"] as! [String : Any]
                let vin = vehicleDict["vin"] as! String
                print("name is \(vin)")
                print("name is \(vinNo)")
                if vin == vinNo {
                    
                    
                    let isAlertActive : Bool = resultDict["is_alert"] as! Bool
                    if isAlertActive == true {
                        
                        
                        let subscriptionId : Int = resultDict["id"] as! Int
                        if subscriptionId != 0 {
                            let parameters = ["is_alert": "False",
                                              "is_email" : "False",
                                              "is_sms" : "False"] as [String : Any]
                            self.changeSubscriptionToTrueOrFalse(method: .patch, parameter: parameters, vinId: subscriptionId)
                            print("unsubscribe")
                        }
                    }
                    else{
                        
                        
                        let subscriptionId : Int = resultDict["id"] as! Int
                        
                        var alertEmail : String = "False"
                        var alertSMS : String = "False"
                        
                        if self.popUp.btnText.tag == 1 {
                            alertSMS = "True"
                        }
                        
                        if self.popUp.btnEmail.tag == 1 {
                            alertEmail = "True"
                        }
                        
                        if subscriptionId != 0 {
                            let parameters = ["is_alert": "True",
                                              "is_email" : alertEmail,
                                              "is_sms" : alertSMS] as [String : Any]
                            self.changeSubscriptionToTrueOrFalse(method: .patch, parameter: parameters, vinId: subscriptionId)

                            print("subscribe")
                        }

                        
                        
                    }
                    
                    
                    break
                }
            }
            
            
            
            
            
            //self.subscribeToAlertsWithVIN(method: .post, parameter: parameters)
            self.dismissPopup()
        }
        
    }
    func dismissPopup() {
        if self.popup != nil {
            self.dismissPopupViewController(animated: true, completion: nil)
        }
    }
    
    @IBAction func viewAction(_ sender: UIButton) {
        capturedCode = vin.value(forKey: "vin") as! String
        print(capturedCode)
        performSegue(withIdentifier: "historyTorecall", sender: nil)
    }
    
    @IBAction func recheckAction(_ sender: UIButton) {
        capturedCode = vin.value(forKey: "vin") as! String
        performSegue(withIdentifier: "historyTorecall", sender: nil)
    }
    @IBAction func removeAction(_ sender: UIButton) {
        
        
        let vehicle  = HistoryArray.object(at: sender.tag) as! NSDictionary
        let itemId : String?
           itemId = "\(vehicle["id"]!)"
        
        //=True
        let parameters = ["is_visible": "False"] as [String : Any]
        self.changeHistoryItem(method: .patch, parameter: parameters, vinId: itemId!)
        
        toolBarView.isHidden = true
        //delete
        HistoryArray.removeObject(at: sender.tag)
        tableView.reloadData()

        
    }
    
    func changeHistoryItem (method : HTTPMethod , parameter : Parameters ,vinId : String) {
        
        SVProgressHUD.show(withStatus: "")
        let StoredToken = UserDefaults.standard
        let  auth_token = StoredToken.string(forKey: "auth_token")
        
        //http headers
        let headers: HTTPHeaders = [
            "Authorization": "Token" + " " + auth_token!,
            "Accept": "application/json"
        ]
        //sending request
        print(vinId)
        print(parameter)
        print("https://app.recallmasters.com/api/v1/lookup/history/\(auth_token!)/\(vinId)/")

        Alamofire.request("https://app.recallmasters.com/api/v1/lookup/history/\(auth_token!)/\(vinId)/" , method: method , parameters : parameter , headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success(let JSON):
                SVProgressHUD.dismiss()
                print("Success with JSON: \(JSON)")
                print(response)
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss()
                SCLAlertView().showError("", subTitle:error.localizedDescription)
                print("Request failed with error: \(error.localizedDescription)")
                break
            }
        }
    }
    
    func subscribeToAlertsWithVIN (method : HTTPMethod , parameter : Parameters) {
        
        SVProgressHUD.show(withStatus: "")
        let StoredToken = UserDefaults.standard
        let  auth_token = StoredToken.string(forKey: "auth_token")
        
        //http headers
        let headers: HTTPHeaders = [
            "Authorization": "Token" + " " + auth_token!,
            "Accept": "application/json"
        ]
        //sending request
        Alamofire.request("https://app.recallmasters.com/api/v1/subscriptions/" , method: method , parameters : parameter , headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success(let JSON):
                SVProgressHUD.dismiss()
                print("Success with JSON: \(JSON)")
                
            
                if let response  = JSON as? NSDictionary {
                    let vehicle = response["vehicle"] as? NSDictionary
                    let vin = vehicle?.value(forKey: "vin")
                    print(vin!)
                    if vin != nil{
                        
                        SCLAlertView().showSuccess("Success", subTitle: "")
                        
                    }
                }
                
               
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss()
                
                SCLAlertView().showError("", subTitle:error.localizedDescription)
                print("Request failed with error: \(error.localizedDescription)")
                break
            }
        }
    }
    
    
    func getVinHistoryWithUDID (requestUrl : String)
    {
        SVProgressHUD.show(withStatus: "")
        let StoredToken = UserDefaults.standard
        let  auth_token = StoredToken.string(forKey: "auth_token")
        
        let headers: HTTPHeaders = [
            "Authorization": "Token" + " " + auth_token!,
        ]
        
        Alamofire.request(requestUrl , method : .get , headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success(let JSON):
                
                let response = JSON as! NSDictionary
                print(response)
                let results = response["results"] as? [[String: Any]]
                let next = response["next"] as? String
               
                if next != nil {
                    self.getVinHistoryWithUDID(requestUrl: next!)
                }
                
                for resulta in results!{
                  self.HistoryArray.add(resulta);
                }
                
                
                self.HistoryArray.sort(using: [NSSortDescriptor(key: "last_time", ascending: false)])
                SVProgressHUD.dismiss()
                if(self.HistoryArray.count == 0) {
                    
                    self.tableView.isHidden = true
                    self.lblNoHistory.isHidden = false
                }
                else{
                    
                    
                    self.checkForAlreadySubscribed()
                    //TODO
                    self.tableView.isHidden = false
                    self.lblNoHistory.isHidden = true
                    self.tableView.reloadData()
                }

                break
            case .failure(let error):
                SVProgressHUD.dismiss()
                
                SCLAlertView().showError("", subTitle:error.localizedDescription)
                print("Request failed with error: \(error.localizedDescription)")
                break
            }
        }
    }
    
    // MARK: - actions
    
    @IBAction func scanAnotherVinAction(_ sender: AnyObject) {
        self.popUpView.isHidden = true
        self.performSegue(withIdentifier: "historyToLoginBack", sender: nil)
        //        self.cont.reset()
        //        present(cont, animated: true, completion: nil)
    }
    
    @IBAction func EnterVinAction(_ sender: AnyObject) {
        self.popUpView.isHidden = true
        performSegue(withIdentifier: "historyTorecall", sender: nil)
    }
    @IBAction func helpAction(_ sender: AnyObject) {
        self.popUpView.isHidden = true
        performSegue(withIdentifier: "historyToHelp", sender: nil)
        
    }
    
    @IBAction func loginAction(_ sender: AnyObject) {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey: "IsFirstTimeLogged")
        self.popUpView.isHidden = true
        let StoredToken = UserDefaults.standard
        StoredToken.setValue(nil, forKey: "txtUsername")
        StoredToken.setValue(nil, forKey: "txtPassword")
        StoredToken.synchronize()
        self.performSegue(withIdentifier: "333", sender: nil)
    }
    
    @IBAction func openPopUpAction(_ sender: AnyObject) {
        if(self.popUpView.isHidden == true) {
            self.popUpView.isHidden=false
        }else{
            
            self.popUpView.isHidden=true
        }
        
    }
    
    @IBAction func openProfileAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "history-profile", sender: nil)
    }
    @IBAction func backAction(_ sender: AnyObject) {
        if isFrom == "home" {
            self.performSegue(withIdentifier: "historyToHome", sender: nil)
            
        }
        else
        {
            self.performSegue(withIdentifier: "historyToLoginBack", sender: nil)
        }
    }
    @IBAction func takeTourAction(_ sender: UIButton) {
        createAndShowTheIntroView()
    }
    // MARK: - methods
    func createAndShowTheIntroView() {
        
        
        
        let page1 = EAIntroPage()
        page1.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page1.titleIconView = UIImageView(image: UIImage(named: "imgCamera")!)
        page1.titleIconView.frame = self.frameView.frame
        page1.title = "Vin Scan Page"
        page1.desc = ""
        
        let page2 = EAIntroPage()
        page2.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page2.titleIconView = UIImageView(image: UIImage(named: "imgRecallCheck-1")!)
        page2.titleIconView.frame = self.frameView.frame
        page2.title = " You can manually scan a VIN"
        page2.desc = ""
        
        let page3 = EAIntroPage()
        page3.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page3.titleIconView = UIImageView(image: UIImage(named: "imgHistory-1")!)
        page3.titleIconView.frame = self.frameView.frame
        page3.title = "History Screen"
        page3.desc = ""
        
        let page4 = EAIntroPage()
        page4.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page4.titleIconView = UIImageView(image: UIImage(named: "alertScreen-1")!)
        page4.titleIconView.frame = self.frameView.frame
        page4.title = ""
        page4.desc = ""
        
        let page8 = EAIntroPage()
        page8.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page8.titleIconView = UIImageView(image: UIImage(named: "cellSwipeScreen-1")!)
        page8.titleIconView.frame = self.frameView.frame
        page8.title = ""
        page8.desc = ""
        
        let intro = EAIntroView(frame: self.view.bounds, andPages: [page1, page2 , page3 , page4 , page8])
        
        intro?.delegate = self
        intro?.show(in: self.view, animateDuration: 0.0)
    }
    
    //    func handleTap(_ sender: UITapGestureRecognizer? = nil) {
    //        // handling code
    //
    //        self.popUpView.isHidden = true
    //    }
    
    
    
    
    
    @IBAction func unwindToHistory(_ segue: UIStoryboardSegue) {
    }
    
    
    
    func checkForAlreadySubscribed () {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
            SVProgressHUD.show(withStatus: "Checking for previous subscriptions")
        }
        let StoredToken = UserDefaults.standard
        let  auth_token = StoredToken.string(forKey: "auth_token")
        print("Token" + " " + auth_token!)
        //http headers
        let headers: HTTPHeaders = [
            "Authorization": "Token" + " " + auth_token!,
            "Accept": "application/json"
        ]
        //sending request
        Alamofire.request("https://app.recallmasters.com/api/v1/subscriptions/" , method: .get , parameters : nil , headers: headers ).responseJSON { response in
            switch response.result {
            case .success(let _):
                SVProgressHUD.dismiss()
                if let json = response.result.value as? [String: Any] {
                    self.allResultsDicts = json["results"] as? [[String:Any]]
                    
                    
                }
                else{
                    print("NO Success")
                    
                }
                break
            case .failure(let error):
                SVProgressHUD.dismiss()
                SCLAlertView().showError("", subTitle:error.localizedDescription)
                print("Request failed with error: \(error.localizedDescription)")
                break
            }
        }
    }
    
    func changeSubscriptionToTrueOrFalse (method : HTTPMethod , parameter : Parameters ,vinId : Int) {
        
        
        SVProgressHUD.show(withStatus: "")
        let StoredToken = UserDefaults.standard
        let  auth_token = StoredToken.string(forKey: "auth_token")
        
        //http headers
        let headers: HTTPHeaders = [
            "Authorization": "Token" + " " + auth_token!,
            "Accept": "application/json"
        ]
        //sending request
        Alamofire.request("https://app.recallmasters.com/api/v1/subscriptions/\(vinId)/" , method: method , parameters : parameter , headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success(let JSON):
                SVProgressHUD.dismiss()
                print("Success with JSON: \(JSON)")
                print(response)
                
                if let json = response.result.value as? [String: Any] {
                   self.checkForAlreadySubscribed()
                    self.dismissPopup()
                }
                
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss()
                
                SCLAlertView().showError("", subTitle:error.localizedDescription)
                print("Request failed with error: \(error.localizedDescription)")
                break
            }
        }
        
    }
    
}
//sample method to barcode profile api data


extension HistoryViewController: BarcodeScannerCodeDelegate,BarcodeScannerErrorDelegate,BarcodeScannerDismissalDelegate {
    
    func barcodeScanner(_ controller: BarcodeScannerController, didCaptureCode code: String, type: String) {
        print(code)
        self.capturedCode = code
        self.capturedCode = self.capturedCode.replacingOccurrences(of: "I", with: "", options: NSString.CompareOptions.literal, range: nil)
        self.popUpView.isHidden = true
        controller.reset()
        self.dismiss(animated: true, completion: {self.performSegue(withIdentifier: "historyTorecall", sender: nil)})
     }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "historyTorecall" {
            let viewController:RecallChekViewController = segue.destination as! RecallChekViewController
            
            viewController.capturedCode = self.capturedCode
            viewController.isFrom = "viewHistoryandDontAddAgain"
            
        }
        if segue.identifier == "history-profile" {
            let viewController:ProfileViewController = segue.destination as! ProfileViewController
            
            viewController.From = "history"
            
        }
        
    }
    /// Delegate to handle the captured code.
    
    func barcodeScanner(_ controller: BarcodeScannerController, didCapturedCode code: String){
        print("Captured number is \(code)")
    }
    
    
    /// Delegate to report errors.
    func barcodeScanner(_ controller: BarcodeScannerController, didReceiveError error: Error){
        
    }
    
    /// Delegate to dismiss barcode scanner when the close button has been pressed.
    func barcodeScannerDidDismiss(_ controller: BarcodeScannerController){
        
        dismiss(animated: true, completion: nil)
        
    }
    @IBAction func unwindToLoginScreen(_ segue: UIStoryboardSegue){
    }
    
}

