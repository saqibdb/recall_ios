//
//  AlertOptions.swift
//  Recall Check
//
//  Created by iBuildx-Mac3 on 3/29/17.
//  Copyright © 2017 iBuildX. All rights reserved.
//

import UIKit

class AlertOptions: UIViewController {

    
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var imgEmail: UIImageView!
    @IBOutlet weak var btnEmail: UIButton!
    
    @IBOutlet weak var imgText: UIImageView!
    @IBOutlet weak var btnText: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
