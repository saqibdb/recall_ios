//
//  HelpViewController.swift
//  RecallMasters VIN Check
//
//  Created by ibuildx on 9/1/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit
import SVProgressHUD
import AMSmoothAlert
import EAIntroView

class HelpViewController: UIViewController ,UIWebViewDelegate , UIGestureRecognizerDelegate ,EAIntroDelegate{
    // MARK: - outlets and variables
    let contr = BarcodeScannerController()
    var capturedCode : String = ""
    
    @IBOutlet weak var recallMonitorBtn: UIButton!
    var isFrom : String = ""
    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var openPopUpBtn: UIButton!
    
    // MARK: - view did load
    override func viewDidLoad() {
        super.viewDidLoad()
        
         UIApplication.shared.statusBarStyle = .lightContent
        contr.codeDelegate = self
        contr.errorDelegate = self
        contr.dismissalDelegate = self
        self.popUpView.isHidden=true
        let url = URL (string: "https://app.recallmasters.com/mobile/help/")
        let requestObj = URLRequest(url: url! as URL)
        webView.loadRequest(requestObj as URLRequest)
        webView.delegate=self
        
        //NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("handleRecallMonitoring"), object: nil)
        
        // popview shadow
        self.popUpView.layer.cornerRadius=2.0
        /*
        self.popUpView.layer.shadowColor = UIColor.black.cgColor
        self.popUpView.layer.shadowOpacity = 0.4
        self.popUpView.layer.shadowOffset = CGSize.zero
        self.popUpView.layer.shadowRadius = 5
        self.popUpView.layer.shouldRasterize = true
        */
        
        
        self.popUpView.layer.masksToBounds = false
        self.popUpView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.popUpView.layer.shadowRadius = 3.0
        self.popUpView.layer.shadowOpacity = 0.7
        
        
        let StoredToken = UserDefaults.standard
        if(( StoredToken.string(forKey: "auth_token")) == "9931b14a2be3944f405a0f8f07eb739c17a00e41"){
            
            loginBtn.setTitle("Log in", for:.normal)
        }
            
        else{
            loginBtn.setTitle("Log Out", for:.normal)
        }
        /*
        if UserDefaults.standard.bool(forKey: "isMonitoring")
        {
            self.recallMonitorBtn.setTitle("Monitoring Recalls", for: UIControlState.normal)
        }
        else{
            
            self.recallMonitorBtn.setTitle("Monitor Recalls", for: UIControlState.normal)
        }
        */

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.webView.addGestureRecognizer(tap)
        tap.delegate=self
    }
    func methodOfReceivedNotification(notification: Notification){
        if UserDefaults.standard.bool(forKey: "isMonitoring")
        {
            self.recallMonitorBtn.setTitle("Monitoring Recalls", for: UIControlState.normal)
        }
        else{
            
            self.recallMonitorBtn.setTitle("Monitor Recalls", for: UIControlState.normal)
        }

    }
    
    @IBAction func recallMonitorAction(_ sender: UIButton) {
        if UserDefaults.standard.bool(forKey: "isMonitoring")
        {
            UserDefaults.standard.set(false, forKey: "isMonitoring")
            self.recallMonitorBtn.setTitle("Monitor Recalls", for: UIControlState.normal)
        }
        else{
            
            UserDefaults.standard.set(true, forKey: "isMonitoring")
            self.recallMonitorBtn.setTitle("Monitoring Recalls", for: UIControlState.normal)
        }
        NotificationCenter.default.post(name: Notification.Name("handleRecallMonitoring"), object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        
    }
       override func viewDidAppear(_ animated: Bool) {
        
            self.popUpView.isHidden = true
        }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
           }
    // MARK: - actions
    @IBAction func scanAnotherVINAction(_ sender: AnyObject) {
        self.popUpView.isHidden = true
        self.performSegue(withIdentifier: "helpToLoginBack", sender: nil)
//        self.contr.reset()
//        present(contr, animated: true, completion: nil)
    }
    @IBAction func enterVINAction(_ sender: AnyObject) {
        self.popUpView.isHidden = true
        performSegue(withIdentifier: "helpTorecall", sender: nil)
        
    }
    
    @IBAction func helpAction(_ sender: AnyObject) {
        self.popUpView.isHidden = true
        self.popUpView.isHidden=true
    }
    
    @IBAction func loginAction(_ sender: AnyObject) {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey: "IsFirstTimeLogged")
        self.popUpView.isHidden = true
        let StoredToken = UserDefaults.standard
        StoredToken.setValue(nil, forKey: "txtUsername")
        StoredToken.setValue(nil, forKey: "txtPassword")
        StoredToken.synchronize()
        self.performSegue(withIdentifier: "222", sender: nil)
    }
    
    @IBAction func openProfileAction(_ sender: UIButton) {
        performSegue(withIdentifier: "help-profile", sender: nil)
    }
    
    
    @IBAction func openPopUpAction(_ sender: AnyObject) {
        if(self.popUpView.isHidden == true) {
            self.popUpView.isHidden=false
        }else{
            
            self.popUpView.isHidden=true
        }

    }
    @IBAction func backAction(_ sender: AnyObject) {
        if isFrom == "home" {
            self.performSegue(withIdentifier: "helpToHome", sender: nil)
         }
        else
        {
            self.performSegue(withIdentifier: "helpToLoginBack", sender: nil)
        }
    }
    @IBAction func takeTourAction(_ sender: UIButton) {
        createAndShowTheIntroView()
    }
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    // MARK: - methods
    func createAndShowTheIntroView() {
        
        
        let page1 = EAIntroPage()
        page1.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page1.titleIconView = UIImageView(image: UIImage(named: "imgCamera")!)
        page1.titleIconView.frame = self.frameView.frame
        page1.title = "Vin Scan Page"
        page1.desc = ""
        
        let page2 = EAIntroPage()
        page2.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page2.titleIconView = UIImageView(image: UIImage(named: "imgRecallCheck-1")!)
        page2.titleIconView.frame = self.frameView.frame
        page2.title = "You can manually scan a VIN"
        page2.desc = ""
        
        let page3 = EAIntroPage()
        page3.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page3.titleIconView = UIImageView(image: UIImage(named: "imgHistory-1")!)
        page3.titleIconView.frame = self.frameView.frame
        page3.title = "History Screen"
        page3.desc = ""
        
        let page4 = EAIntroPage()
        page4.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page4.titleIconView = UIImageView(image: UIImage(named: "alertScreen-1")!)
        page4.titleIconView.frame = self.frameView.frame
        page4.title = ""
        page4.desc = ""
        
        let page8 = EAIntroPage()
        page8.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page8.titleIconView = UIImageView(image: UIImage(named: "cellSwipeScreen-1")!)
        page8.titleIconView.frame = self.frameView.frame
        page8.title = ""
        page8.desc = ""
        
        let intro = EAIntroView(frame: self.view.bounds, andPages: [page1, page2 , page3 , page4 , page8])
        intro?.delegate = self
        intro?.show(in: self.view, animateDuration: 0.0)
    }

    func gestureRecognizer(_: UIGestureRecognizer,  shouldRecognizeSimultaneouslyWith shouldRecognizeSimultaneouslyWithGestureRecognizer:UIGestureRecognizer) -> Bool
    {
        self.popUpView.isHidden = true
        //print("tap found")
        return true
    }
    func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        
        self.popUpView.isHidden = true
    }

    @IBAction func unwindToHelp(_ segue: UIStoryboardSegue) {
    }
    
}
// sample method to barcode profile api data


extension HelpViewController: BarcodeScannerCodeDelegate,BarcodeScannerErrorDelegate,BarcodeScannerDismissalDelegate {
    
    func barcodeScanner(_ controller: BarcodeScannerController, didCaptureCode code: String, type: String) {
        print(code)
        self.capturedCode = code
        self.capturedCode = self.capturedCode.replacingOccurrences(of: "I", with: "", options: NSString.CompareOptions.literal, range: nil)
        self.popUpView.isHidden = true
        controller.reset()
        self.dismiss(animated: true) { 
            self.performSegue(withIdentifier: "helpTorecall", sender: nil)
        }
      }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "helpTorecall" {
            let viewController:RecallChekViewController = segue.destination as! RecallChekViewController
            
            viewController.capturedCode = self.capturedCode
            
        }
        if segue.identifier == "help-profile" {
            let viewController:ProfileViewController = segue.destination as! ProfileViewController
            
            viewController.From = "help"
            
        }

        
    }
    /// Delegate to handle the captured code.
    
    func barcodeScanner(_ controller: BarcodeScannerController, didCapturedCode code: String){
        print("Captured number is \(code)")
    }
    
    
    /// Delegate to report errors.
    func barcodeScanner(_ controller: BarcodeScannerController, didReceiveError error: Error){
        
    }
    
    /// Delegate to dismiss barcode scanner when the close button has been pressed.
    func barcodeScannerDidDismiss(_ controller: BarcodeScannerController){
        
        dismiss(animated: true, completion: nil)
        
    }
    @IBAction func unwindToLoginScreen(_ segue: UIStoryboardSegue) {
    }
    
}


