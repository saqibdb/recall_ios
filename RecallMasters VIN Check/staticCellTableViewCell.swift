//
//  staticCellTableViewCell.swift
//  Recall Check
//
//  Created by ibuildx on 12/22/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit

class staticCellTableViewCell: UITableViewCell {

    
    @IBOutlet weak var subscribeToAlertBtn: UIButton!
    @IBOutlet weak var txtVIN: UITextField!
    @IBOutlet weak var lblCharactersCount: UILabel!
    @IBOutlet weak var mainVehicleInfoView: UIView!
    @IBOutlet weak var lblVehiclesFound: UILabel!
    @IBOutlet weak var vehicleInfiView: UIView!
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var lblRecalls2: UILabel!
    @IBOutlet weak var recallCheckBTn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
