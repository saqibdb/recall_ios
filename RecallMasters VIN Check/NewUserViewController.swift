//
//  NewUserViewController.swift
//  Recall Check
//
//  Created by ibuildx on 11/4/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit

class NewUserViewController: UIViewController {

    
    
    @IBOutlet weak var personalBtn: UIButton!
    @IBOutlet weak var buisnessBtn: UIButton!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()

       UIApplication.shared.statusBarStyle = .default
        //self.personalBtn.layer.cornerRadius=4
        //self.buisnessBtn.layer.cornerRadius=4


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    @IBAction func backAction(_ sender: UIButton) {
        
        
        
          self.performSegue(withIdentifier: "newUserToLogin", sender: nil)
    }
    @IBAction func unwindToNewUserViewController(_ segue: UIStoryboardSegue) {
    }
}
