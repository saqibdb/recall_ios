//
//  BuisnessSubscriptionViewController.swift
//  Recall Check
//
//  Created by ibuildx on 11/9/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit
import SCLAlertView
import Alamofire
import SVProgressHUD
class BuisnessSubscriptionViewController: UIViewController {
    
    
    @IBOutlet weak var radioYes: UIButton!
    @IBOutlet weak var radioNo: UIButton!
    @IBOutlet weak var imgYes: UIImageView!
    @IBOutlet weak var imgNo: UIImageView!
    var radioSelection : String = "YES"
    var CompanyName : String = ""
    @IBOutlet weak var txtSubscriptionCode: UITextField!
    @IBOutlet weak var continueBTN: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .default
      
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func continueAction(_ sender: UIButton) {
        
        if(self.radioSelection == "YES" && self.txtSubscriptionCode.text == ""){
            SCLAlertView().showError("", subTitle: "Please enter a verification code")
        }
        else if(self.radioSelection == "YES" && (self.txtSubscriptionCode.text?.characters.count)! > 6){
            SCLAlertView().showError("", subTitle: "Ensure the verification code has no more than 6 digits")
          }
        else if (self.radioSelection == "No"){
            self.CompanyName = ""
            self.performSegue(withIdentifier: "subCodeToBznsAcount", sender: nil)
        }
        else{
            SVProgressHUD.show()
            //Get request to check validity of subsucription Code
            Alamofire.request("https://app.recallmasters.com/api/v1/auth/validate_subscription_code/\(self.txtSubscriptionCode.text!)/", method: .get).responseJSON { response in
                
                switch response.result {
                case .success(let JSON):
                    SVProgressHUD.dismiss()
                    let response = JSON as! NSDictionary
                    self.CompanyName = (response.object(forKey: "company")! as? String)!
                   
                    self.performSegue(withIdentifier: "subCodeToBznsAcount", sender: nil)
                    break
                case .failure(let error):
                    SVProgressHUD.dismiss()

                    SCLAlertView().showError("", subTitle: "Invalid verification code!")
                    print("Request failed with error: \(error.localizedDescription)")
                    break
                }
            }
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "subToNewUserBack", sender: nil)
    }
    @IBAction func  unwindTosubscription(_ segue: UIStoryboardSegue) {
    }
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    @IBAction func RadioAction(_ sender: UIButton) {
        
        if sender.tag == 11 {
            
            imgYes.image = imageNamed("radioFill")
            imgNo.image = imageNamed("radioEmpty")
            print("yes")
            self.radioSelection = "YES"
            
        }else if sender.tag == 22 {
            
            imgYes.image = imageNamed("radioEmpty")
            imgNo.image = imageNamed("radioFill")
            print("No")
            self.radioSelection = "No"
        }
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "subCodeToBznsAcount" {
            let viewController:CreateBuisnessAccountViewController = segue.destination as! CreateBuisnessAccountViewController
            
            viewController.subCode = self.txtSubscriptionCode.text!
            if self.radioSelection == "YES" {
                viewController.companyName = self.CompanyName
            }
            else
            {
                viewController.companyName = "No"
            }
            
            
        }
    }
    
    
}
