//
//  AccountCreationSuccessfullViewController.swift
//  Recall Check
//
//  Created by ibuildx on 11/6/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit
import EAIntroView


class AccountCreationSuccessfullViewController: UIViewController ,EAIntroDelegate{

    
    @IBOutlet weak var bulletOne: UILabel!
    let controller = BarcodeScannerController()

    @IBOutlet weak var lblScanVin: UILabel!
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var bulletTwo: UILabel!
    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var scanFirstBTN: UIButton!
    @IBOutlet weak var takeAtourBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .default
        if (UserDefaults.standard.bool(forKey: "IsFirstTimeLogged") == true)
        {
           self.lblWelcome.text = "Account Creation Successful"
            self.lblScanVin.text = "SCAN YOUR FIRST VIN"
         }
        else
        {
           self.lblWelcome.text = "Welcome Back!"
            self.lblScanVin.text = "SCAN ANOTHER VIN"
        }               //self.scanFirstBTN.layer.cornerRadius=4
        //self.takeAtourBtn.layer.cornerRadius=4
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        if (UserDefaults.standard.bool(forKey: "IsFirstTimeLogged") == true)
        {
            self.createAndShowTheIntroView()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
       func createAndShowTheIntroView() {
      
        let page1 = EAIntroPage()
        page1.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page1.titleIconView = UIImageView(image: UIImage(named: "imgCamera")!)
        page1.titleIconView.frame = self.frameView.frame
        page1.title = "Vin Scan Page"
        page1.desc = "Align the red line to the vehicles VIN barcode and wait for the scanner to sense the barcode."
        
        let page2 = EAIntroPage()
        page2.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page2.titleIconView = UIImageView(image: UIImage(named: "imgRecallCheck-1")!)
        page2.titleIconView.frame = self.frameView.frame
        page2.title = " Manual VIN Recall Check Page "
        page2.desc = "Manually enter your vehicle identification number and press the Recall Check button."
        
        let page3 = EAIntroPage()
        page3.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page3.titleIconView = UIImageView(image: UIImage(named: "imgHistory-1")!)
        page3.titleIconView.frame = self.frameView.frame
        page3.title = "History Screen"
        page3.desc = "Click on the vehicle box to see the full menu and select an option from the toolbar for each vehicle to make a change.  "
        
        let page4 = EAIntroPage()
        page4.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page4.titleIconView = UIImageView(image: UIImage(named: "alertScreen-1")!)
        page4.titleIconView.frame = self.frameView.frame
        page4.title = "Alerts"
        page4.desc = "Tap on Alerts to get latest alerts (Notifications) about the listed vehicles recall changes"
        
        //new
        let page5 = EAIntroPage()
        page5.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page5.titleIconView = UIImageView(image: UIImage(named: "viewScreen")!)
        page5.titleIconView.frame = self.frameView.frame
        page5.title = "View"
        page5.desc = "Tap on the View box to view detailed information about the vehicle recall history"
        
        let page6 = EAIntroPage()
        page6.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page6.titleIconView = UIImageView(image: UIImage(named: "recheckScreen")!)
        page6.titleIconView.frame = self.frameView.frame
        page6.title = "Recheck"
        page6.desc = "Tap on the Recheck box to recheck a new VIN number of any other Vehicle"
        
        let page7 = EAIntroPage()
        page7.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page7.titleIconView = UIImageView(image: UIImage(named: "removeScreen")!)
        page7.titleIconView.frame = self.frameView.frame
        page7.title = "Remove"
        page7.desc = "Tap on the Remove box to remove a selected item from the history list"
        
        let page8 = EAIntroPage()
        page8.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page8.titleIconView = UIImageView(image: UIImage(named: "cellSwipeScreen-1")!)
        page8.titleIconView.frame = self.frameView.frame
        page8.title = ""
        page8.desc = "Slide left to quick view of previous recall check or tap on remove to remove item from the list "
        
        
        let intro = EAIntroView(frame: self.view.bounds, andPages: [page1, page2 , page3 , page4 ,page5, page6 , page7 , page8])
        intro?.delegate = self
        intro?.show(in: self.view, animateDuration: 0.0)
    }
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    @IBAction func takeAtourAction(_ sender: UIButton) {
        
         self.createAndShowTheIntroView()
    }
    
    
    @IBAction func scanYourFirstVINaction(_ sender: UIButton) {
        
        
       
         self.performSegue(withIdentifier: "creationToBarcode", sender: nil)

    }
    @IBAction func unwindToAccountCreation(_ segue: UIStoryboardSegue) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
