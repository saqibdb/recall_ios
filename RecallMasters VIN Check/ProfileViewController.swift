//
//  ProfileViewController.swift
//  Recall Check
//
//  Created by ibuildx on 12/21/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import SVProgressHUD
class ProfileViewController: UIViewController {
    
  
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtRepeatPassword: UITextField!
    public var From : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .default
        self.getAndUpdateProfileData(method: .get, parameter:["":""] )
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backAction(_ sender: AnyObject) {
        
        if From == "EnterVin" {
            performSegue(withIdentifier: "profile-recall", sender: self)
            
        }
        else if From == "help" {
        performSegue(withIdentifier: "profileToHelp", sender: self)
            
        }
        else if From == "history"{
        performSegue(withIdentifier: "profileToHistory", sender: self)
            
        }
        else if From == "home"{
            performSegue(withIdentifier: "profileToHome", sender: self)
        
        }
        else {
        performSegue(withIdentifier: "ProfileToScanner", sender: self)
        }
     }
    
    func getAndUpdateProfileData (method : HTTPMethod , parameter : Parameters)
    {
        SVProgressHUD.show(withStatus: "")
        let StoredToken = UserDefaults.standard
        let  auth_token = StoredToken.string(forKey: "auth_token")
        
        //http headers
        let headers: HTTPHeaders = [
            "Authorization": "Token" + " " + auth_token!,
            "Accept": "application/json"
        ]
        //sending request
        Alamofire.request("https://app.recallmasters.com/api/v1/auth/me/" , method: method , parameters : parameter , headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success(let JSON):
                SVProgressHUD.dismiss()
                print("Success with JSON: \(JSON)")
                //setting text in fields
                let response = JSON as! NSDictionary
                
                self.txtFirstName.text = response.object(forKey: "first_name")! as? String
                self.txtLastName.text = response.object(forKey: "last_name")! as? String
                self.txtEmail.text = response.object(forKey: "email")! as? String
                self.txtMobile.text = response.object(forKey: "phone")! as? String
                self.txtUserName.text = response.object(forKey: "username")! as? String
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss()
                
                SCLAlertView().showError("", subTitle:error.localizedDescription)
                print("Request failed with error: \(error.localizedDescription)")
                break
            }
        }
        
    }
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    func updateProfileData()
    {
        //parameters
        let parameters : Parameters = ["company":"" , "email": self.txtEmail.text!,"first_name":self.txtFirstName.text! , "id":"","last_name":self.txtLastName.text! , "phone":self.txtMobile.text!,"username":self.txtUserName.text! ]
        
        if(self.txtMobile.text == ""){
            
            SCLAlertView().showError("phone", subTitle:"This field may not be blank")
        }
        else{
            self.getAndUpdateProfileData(method: .patch, parameter: parameters)
        }
    }
    
    @IBAction func upadateAction(_ sender: UIButton) {
        self.updateProfileData()
    }
    @IBAction func unwindToProfile(_ segue: UIStoryboardSegue) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "profileToTerms" {
            let viewController:TermsAndConditionsViewController = segue.destination as! TermsAndConditionsViewController
            
            viewController.isFrom = "profile"
            
        }
        
        
    }
    
    
}
