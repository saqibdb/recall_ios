//
//  HistoryCell.swift
//  RecallMasters VIN Check
//
//  Created by ibuildx on 10/9/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit

class HistoryCell: MGSwipeTableCell {

   
   
    
    @IBOutlet weak var lblCounts: UILabel!
    @IBOutlet weak var lblVhecleName: UILabel!
    @IBOutlet weak var lblRecallsCount: UILabel!
    @IBOutlet weak var lblVin: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
