//
//  CreateBuisnessAccountViewController.swift
//  Recall Check
//
//  Created by ibuildx on 11/6/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit
import SCLAlertView
import SVProgressHUD
class CreateBuisnessAccountViewController: UIViewController {
    
    
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var txtBuisnessName: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtRequestedUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtRepeatPassword: UITextField!
    var subCode : String = ""
    var companyName : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.subCode)
        UIApplication.shared.statusBarStyle = .default
        if self.companyName == "No" {
            
        }
        else
        {
             self.txtBuisnessName.text =  self.companyName
        }
       
        print(self.companyName);
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    @IBAction func registerAction(_ sender: UIButton) {
        
        if self.companyName == "No" {
            
            let trialTimeStartDate = UserDefaults.standard
            trialTimeStartDate.set(NSDate(), forKey: "LimitReachedOnDate")
            
            // retrieve from user defaults
            let limitDate = trialTimeStartDate.object(forKey: "LimitReachedOnDate") as? NSDate
            print(limitDate)
        }
        
        self.sendRegisterRequest()
    }
    
    
    @IBAction func termsAndConditionsAction(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "businessToTermCondVC", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "businessToTermCondVC" {
            let viewController:TermsAndConditionsViewController = segue.destination as! TermsAndConditionsViewController
            
            viewController.isFrom = "businessVC"
            
        }
        
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "detatilToCode", sender: nil)
    }
    func sendRegisterRequest () {
        //checking required fields  on application end
        let range = self.txtRequestedUserName.text?.rangeOfCharacter(from: NSCharacterSet.whitespaces)
        let emailReg: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailReg)
        
        if emailTest.evaluate(with: self.txtEmail.text!) == false {
            SCLAlertView().showError("", subTitle: "Enter a valid email address")
        }
            
            
        else if(self.txtRequestedUserName.text == "") {
            
            SCLAlertView().showError("", subTitle: "A username is required")
        }
        else if(self.txtPassword.text == "" || self.txtRepeatPassword.text == ""){
            if(self.txtPassword.text == "") {
                
                SCLAlertView().showError("", subTitle: "A password is required")
            }
            else if(self.txtRepeatPassword.text == "") {
                
                SCLAlertView().showError("", subTitle: "Please confirm your password")
            }
        }
            //checking matching passswords on application end
        else if(txtPassword.text != txtRepeatPassword.text){
            
            SCLAlertView().showError("", subTitle: "The requested passwords are not matching")
            
        }
        else if range != nil {
            SCLAlertView().showError("", subTitle: "Enter a valid username. This value may contain only letters, numbers and @/./+/-/_ characters.")
        }
            
        else{
            SVProgressHUD.show()
            print(self.txtMobileNo.text!)
            self.post(["username":self.txtRequestedUserName.text! , "password":self.txtPassword.text! , "email":self.txtEmail.text! , "phone":self.txtMobileNo.text! , "first_name":self.txtFirstName.text! , "last_name":self.txtLastName.text! , "subscription_code":self.subCode], url: "https://app.recallmasters.com/api/v1/auth/register/")
            //do the entire request here
            
        }
        
    }
    func post(_ params : Dictionary<String,String>, url : String) {
        let request = NSMutableURLRequest(url: URL(string: url)!)
        _ = URLSession.shared
        request.httpMethod = "POST"
        
        do
        {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        }
        catch {
            //handle error. Probably return or mark function as throws
            print(error)
            return
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            // handle error
            
            guard error == nil else {
                SVProgressHUD.dismiss()
                //let err = error?.description
                OperationQueue.main.addOperation {
                    SCLAlertView().showError("Sorry", subTitle:"The internet connection appears to be offline!")
                }
                
                return
                
                
            }
            
            let json: NSDictionary?
            do
            {
                json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? NSDictionary
            }
            catch let dataError {
                SVProgressHUD.dismiss()
                
                // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                print(dataError)
                
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error could not parse JSON: '\(jsonStr)'")
                print(jsonStr)
                // return or throw?
                return
            }
            
            print(json!)
            if((json!["username"] as? String) != nil)
            {
                SVProgressHUD.dismiss()
                
                let date = Date()
                let startTimeInterval = UserDefaults.standard
                startTimeInterval.set(date, forKey: "startDate")
                
                OperationQueue.main.addOperation {
                    let alertView = SCLAlertView()
                    alertView.addButton("Continue", target: self, selector:#selector(self.goToLogin))
                    
                    alertView.showSuccess("", subTitle: "Account Created Successfully")
                    
                }
                
            }
            else{
                SVProgressHUD .dismiss()
                if (json!["non_field_errors"] as? [String] != nil){
                    var a : Array = (json!["non_field_errors"] as? [String])!
                    OperationQueue.main.addOperation {
                        SCLAlertView().showError("", subTitle: a[0])
                    }
                }
                else {
                    OperationQueue.main.addOperation {
                        SCLAlertView().showError("", subTitle: "The username must be unique")
                    }
                }
                
            }
        }
        
        task.resume()
    }
    
    func goToLogin() {
        UserDefaults.standard.set(true, forKey: "IsFirstTimeLogged")
        self.performSegue(withIdentifier: "businessToLoginBack", sender: nil)
    }
    @IBAction func unwindToCreateBusinessAccountViewController(_ segue: UIStoryboardSegue) {
    }
}
