//
//  RecallChekViewController.swift
//  RecallMasters VIN Check
//
//  Created by ibuildx on 9/1/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//
import Foundation
import UIKit
import SVProgressHUD
import AMSmoothAlert
import SCLAlertView
import Alamofire
import EAIntroView


class CustomClass {
    
    
    var recalls : [[String : AnyObject]] = [[String : AnyObject]]()
    var VinNo : String = ""
    var model_name : String = ""
    var make : String = ""
    var model_year : String = ""
    var recall_count : Int = 0
    var vin : Vins = Vins()
    
}


class RecallChekViewController: UIViewController , UITextFieldDelegate, UIGestureRecognizerDelegate , UITableViewDataSource, UITableViewDelegate ,EAIntroDelegate{
    // MARK: - Outlets and variables
    let controller = BarcodeScannerController()
    var cellA : staticCellTableViewCell =  staticCellTableViewCell ()
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var openPopupBtn: UIButton!
    
    @IBOutlet weak var monitorRecallBtn: UIButton!
    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var popUpView: UIView!
    var isFrom : String = ""
    var vinText : String = ""
    let count = 0
    var modelName : String = ""
    var make : String = ""
    var modelyear : String = ""
    var capturedCode : String = ""
    var recalls_array: NSArray = NSArray()
    var History_array: NSMutableArray = NSMutableArray()
    
    var userEmail : String = ""
    var userPhone : String = ""

    
    var customClass : CustomClass = CustomClass()
    
    
    
    var popUp = AlertOptions()
    
    
    
    var alertBtn  : UIButton!

    
    
    var foundDict : [String:Any]!
    
    
    var isalreadySubscribed : Bool = false
    
    // MARK: - viewdidload
    override func viewDidLoad() {
        
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .lightContent
        controller.codeDelegate = self
        controller.errorDelegate = self
        controller.dismissalDelegate = self
        
        self.tableView.dataSource=self
        self.tableView.delegate=self
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        self.popUpView.isHidden=true
        self.popUpView.layer.cornerRadius=2.0
        
        /*
        self.popUpView.layer.shadowColor = UIColor.black.cgColor
        self.popUpView.layer.shadowOpacity = 0.4
        self.popUpView.layer.shadowOffset = CGSize.zero
        self.popUpView.layer.shadowRadius = 5
        self.popUpView.layer.shouldRasterize = true
         */
        
        self.popUpView.layer.masksToBounds = false
        self.popUpView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.popUpView.layer.shadowRadius = 3.0
        self.popUpView.layer.shadowOpacity = 0.7
        
        self.tableView.register(UINib(nibName: "RecallTableViewCell", bundle: nil), forCellReuseIdentifier: "RecallCell")
        
        let freeToken = "9931b14a2be3944f405a0f8f07eb739c17a00e41"
        let StoredToken = UserDefaults.standard
        
         //NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("handleRecallMonitoring"), object: nil)
        
        if(( StoredToken.string(forKey: "auth_token")) != nil && ( StoredToken.string(forKey: "auth_token")) != freeToken){
            
            loginBtn.setTitle("Log out", for:UIControlState())
        }
        else{
            StoredToken.setValue(freeToken, forKey: "auth_token")
            loginBtn.setTitle("Log in", for:UIControlState())
        }
        
        
        //if segue is from barcode scanner
        
        if(capturedCode != ""){
            //self.capturedCode = self.capturedCode.stringByReplacingOccurrencesOfString("I", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            
            if(self.capturedCode[self.capturedCode.startIndex] == "I"){
                
                self.capturedCode.remove(at: self.capturedCode.startIndex)
                
            }
            
            SVProgressHUD.dismiss()
            self.getRequest()
            
            
            
        }
        self.getProfileData(method: .get, parameter:["":""] )

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        
        /*
        if UserDefaults.standard.bool(forKey: "isMonitoring")
        {
            self.monitorRecallBtn.setTitle("Monitoring Recalls", for: UIControlState.normal)
        }
        else{
           
            self.monitorRecallBtn.setTitle("Monitor Recalls", for: UIControlState.normal)
        }
         */
        
    }
    // MARK: - Orientation
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }

    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        self.popUpView.isHidden = true
    }
    func getRequest() {
        
        let spaceCount = self.capturedCode.characters.filter{$0 == " "}.count
        
        if (self.capturedCode == "" || spaceCount >= 1){
            SVProgressHUD.dismiss()
            SCLAlertView().showError("", subTitle: "The VIN information provided is invalid!")
        }
        else {
            
            let VIN = self.capturedCode
            let StoredToken = UserDefaults.standard
            let  auth_token = StoredToken.string(forKey: "auth_token")
            let final_token = "Token" + " " + auth_token!
            //let deviceID = UIDevice.current.identifierForVendor!.uuidString
            
            // Send HTTP GET Request
           

            let scriptUrl = "https://app.recallmasters.com/api/v1/lookup/\(VIN)/?device_id=\(auth_token!)&use_type=rm_mobile_android&minimum_rank=2"
          
            // let scriptUrl = "https://app.recallmasters.com/api/v1/lookup/" + VIN + "/?device_id=" + auth_token! + "/?format=json"
            let myUrl = URL(string: scriptUrl);
            let request = NSMutableURLRequest(url:myUrl!);
            request.httpMethod = "GET"
            
            request.addValue(final_token, forHTTPHeaderField: "Authorization")
            
            // Excute HTTP Request
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                // Check for error
                if error != nil
                {
                    SVProgressHUD.dismiss()
                    //let err = error?.description
                    OperationQueue.main.addOperation {
                        SCLAlertView().showError("Sorry", subTitle:"The internet connection appears to be offline!")
                    }
                    
                    return
                }
                // Convert server json response to NSDictionary
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        print("%@",convertedJsonIntoDict)
                        // Get value by key
                        let status = convertedJsonIntoDict["status"] as? String
                        
                        DispatchQueue.main.async(execute: {
                            if (status == "ok"){
                                
                                if((convertedJsonIntoDict["error_description"] as? String) == "Unknown vendor")
                                {
                                    SVProgressHUD.dismiss()
                                    SCLAlertView().showError("", subTitle: "The VIN information provided is invalid!")
                                }
                                else if((convertedJsonIntoDict["vin"] as? String) != nil)
                                {
                                    
                                    SVProgressHUD.dismiss()
                                    
                                    self.recalls_array  = convertedJsonIntoDict["recalls"]! as! [[String : AnyObject]] as NSArray
                                    
                                    let vin: Vins = Vins()
                                    vin.vinNo = convertedJsonIntoDict["vin"] as? String
                                    
                                    
                                    self.tableView.reloadData()
                                    
                                    
                                    if ((convertedJsonIntoDict["model_name"] as? String) != nil)
                                    {self.customClass.model_name = convertedJsonIntoDict["model_name"] as! String
                                        
                                        
                                    }
                                    
                                    if((convertedJsonIntoDict["make"] as? String) != nil)
                                    { self.customClass.make = convertedJsonIntoDict["make"] as! String
                                    }
                                    
                                    //if((convertedJsonIntoDict["model_year"] as? String) != nil)
                                    //{
                                    self.customClass.model_year = String (describing: convertedJsonIntoDict["model_year"]!)
                                    // }
                                    
                                    
                                    
                                    self.customClass.recall_count = convertedJsonIntoDict["recall_count"] as! Int
                                    
                                    let recal_count:Int = convertedJsonIntoDict["recall_count"] as! Int
                                    let myUInt32 = Int32(truncatingBitPattern: recal_count)
                                    
                                    // "recall_last_updated" = "2016-11-07T13:21:32.531Z"
                                    
                                    vin.recallCount = myUInt32
                                    let date = Date()
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "EEE, dd MMM yyyy"
                                    let str = dateFormatter.string(from: date)
                                    
                                    vin.time = str
                                    
                                    //self.lblVehicleName.text = String (self.modelyear) + " " + self.make + " " + self.modelName
                                    
                                    vin.vehicleName = "\(self.customClass.model_year) \(self.customClass.make) \(self.customClass.model_name)"
                                    
                                    if self.isFrom == "viewHistoryandDontAddAgain"{
                                        print("from histroy to view the VIN")
                                    }
                                    else{
                                        vin.saveToDb()
                                    }
                                    
                                    self.customClass.vin = vin
                                    
                                    
                                    
                                    
                                    
                                    
                                    self.checkForAlreadySubscribed()
                                    //TODO call the already registered system here
                                    
                                    
                                    self.tableView.reloadData()
                                }
                                else{
                                    SVProgressHUD.dismiss()
                                    SCLAlertView().showError("" , subTitle: "The VIN information provided is invalid!");
                                }
                            }
                                
                            else if(status == "error" || status == "invalid"){
                                SVProgressHUD.dismiss()
                                let error_description = convertedJsonIntoDict["error_description"] as? String
                                SCLAlertView().showError("", subTitle: error_description!)
                                
                                print(error_description!)
                            }
                            
                        })
                        
                        
                    }
                } catch let error as NSError {
                    SVProgressHUD.dismiss()
                    print(error.localizedDescription)
                }
                
            }
            task.resume()
            
        }
    }
    
    func checkForAlreadySubscribed () {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
            SVProgressHUD.show(withStatus: "Checking for previous subscriptions")
        }
        let StoredToken = UserDefaults.standard
        let  auth_token = StoredToken.string(forKey: "auth_token")
        print("Token" + " " + auth_token!)
        //http headers
        let headers: HTTPHeaders = [
            "Authorization": "Token" + " " + auth_token!,
            "Accept": "application/json"
        ]
        //sending request
        Alamofire.request("https://app.recallmasters.com/api/v1/subscriptions/" , method: .get , parameters : nil , headers: headers ).responseJSON { response in
            switch response.result {
            case .success(let _):
                SVProgressHUD.dismiss()
                if let json = response.result.value as? [String: Any] {
                    let allResultsDicts = json["results"] as? [[String:Any]]
                    for resultDict : [String:Any] in allResultsDicts!{
                        let vehicleDict : [String:Any] = resultDict["vehicle"] as! [String : Any]
                        let vin = vehicleDict["vin"] as! String
                        print("name is \(vin)")
                        print("name is \(self.customClass.vin.vinNo!)")
                        if vin == self.customClass.vin.vinNo! {
                            self.isalreadySubscribed = true
                            self.foundDict = resultDict
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                            break
                        }
                    }
                    
                }
                else{
                    print("NO Success")

                }
                break
            case .failure(let error):
                SVProgressHUD.dismiss()
                SCLAlertView().showError("", subTitle:error.localizedDescription)
                print("Request failed with error: \(error.localizedDescription)")
                break
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func methodOfReceivedNotification(notification: Notification){
        if UserDefaults.standard.bool(forKey: "isMonitoring")
        {
            self.monitorRecallBtn.setTitle("Monitoring Recalls", for: UIControlState.normal)
        }
        else{
            
            self.monitorRecallBtn.setTitle("Monitor Recalls", for: UIControlState.normal)
        }
        
    }

    @IBAction func monitorRecallAction(_ sender: UIButton) {
        
        if UserDefaults.standard.bool(forKey: "isMonitoring")
        {
            UserDefaults.standard.set(false, forKey: "isMonitoring")
            self.monitorRecallBtn.setTitle("Monitor Recalls", for: UIControlState.normal)
        }
        else{
            
            UserDefaults.standard.set(true, forKey: "isMonitoring")
            self.monitorRecallBtn.setTitle("Monitoring Recalls", for: UIControlState.normal)
        }
        
        NotificationCenter.default.post(name: Notification.Name("handleRecallMonitoring"), object: nil)
    }
    
    // MARK: - textfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let cell = textField.superview?.superview as! staticCellTableViewCell
        
        
        var str = textField.text!.characters.count + 1
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92){
            
            str =  textField.text!.characters.count - 1
            if (str > 17){
                
                cell.txtVIN.backgroundColor = UIColor(
                    red: 251.0/255.0,
                    green: 102.0/255.0,
                    blue: 68.0/255.0,
                    alpha: 1.0)
            }
            else{
                cell.txtVIN.backgroundColor = UIColor.clear
            }
            
        }
        else{
            if (str > 17 ){
                
                cell.txtVIN.backgroundColor = UIColor(
                    red: 251.0/255.0,
                    green: 102.0/255.0,
                    blue: 68.0/255.0,
                    alpha: 1.0)
            }
            else{
                cell.txtVIN.backgroundColor = UIColor.clear
            }
        }
        
        cell.lblCharactersCount.text = String(str) + " " + "/" + " " + "17"
        capturedCode = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        if capturedCode.characters.count > 0 && str <= 17 {
            if capturedCode[capturedCode.index(capturedCode.startIndex, offsetBy: 0)] == "1" {
                
                cell.txtVIN.backgroundColor = UIColor(
                    red: 251.0/255.0,
                    green: 102.0/255.0,
                    blue: 68.0/255.0,
                    alpha: 1.0)
            }
            else{
                
                cell.txtVIN.backgroundColor = UIColor.clear
            }
        }
        return true
    }
    
    // MARK: - tableview delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recalls_array.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if ((indexPath as NSIndexPath).row == 0) {
            
            let CellIdentifier = "staticCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as! staticCellTableViewCell
            
            
            cell.txtVIN.text =  self.capturedCode
            cell.lblVehiclesFound.text = "\(self.customClass.recall_count)"
            cell.txtVIN.delegate = self
            cell.lblVehicleName.text = "\(self.customClass.model_year) \(self.customClass.make) \(self.customClass.model_name)"
            
            
            
            if(self.customClass.model_year == "" &&  self.customClass.make == "" && self.customClass.model_name == ""   )
            {
                cell.mainVehicleInfoView.isHidden = true
            }
            else
            {
                cell.mainVehicleInfoView.isHidden = false
            }
            
            cell.recallCheckBTn.addTarget(self, action: #selector(recalCheckAction), for: .touchUpInside)
             cell.subscribeToAlertBtn.addTarget(self, action: #selector(alertBtnTapped), for: .touchUpInside)
        
            if self.isalreadySubscribed == true {
                
                let isAlertActive : Bool = self.foundDict["is_alert"] as! Bool
                
                if isAlertActive == true {
                    cell.subscribeToAlertBtn.setBackgroundImage(UIImage(named: "checkBtn"), for: UIControlState.normal)
                }
                else{
                    cell.subscribeToAlertBtn.setBackgroundImage(UIImage(named: "uncheckBtn"), for: UIControlState.normal)
                }
                
            }
            else{
                cell.subscribeToAlertBtn.setBackgroundImage(UIImage(named: "uncheckBtn"), for: UIControlState.normal)
            }
            
            alertBtn = cell.subscribeToAlertBtn
            
            
            
            if (self.customClass.recall_count == 0) {
                cell.lblVehiclesFound.isHidden = true
                cell.lblRecalls2.text = "Zero recalls found"
            }
            else if (self.customClass.recall_count == 1) {
                
                cell.lblVehiclesFound.isHidden = false
                cell.lblVehiclesFound.text =  String(self.customClass.recall_count)
                cell.lblRecalls2.text = "recall found"
            }
            else{
                cell.lblVehiclesFound.isHidden = false
                cell.lblVehiclesFound.text =  String(self.customClass.recall_count)
                cell.lblRecalls2.text = "recalls found"
            }
            
            
            return cell
            
        }
        
        //--------------------second cell--------------------
        
        
        
        let CellIdentifier = "RecallCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as! RecallTableViewCell
        
        let recall = recalls_array.object(at: (indexPath as NSIndexPath).row - 1) as! NSDictionary
        
        
        
        
        let userType = UserDefaults.standard
        let  userTypeString = userType.string(forKey: "userType")
        
        if(userTypeString == "MOBILE_PERSONAL"){
            cell.profitScoreAndAgeTopSpace.constant = -60
            cell.labourMinMaxTopSpace.constant = -185
            
        }
        else{
            cell.profitScoreAndAgeTopSpace.constant = 1
            cell.labourMinMaxTopSpace.constant = 1
            
        }

        //risk rank in header
        if(recall["risk_rank"] as! Int == 1)
        {cell.Risk_rank_header.text = "ZERO" }
            
        else if(recall["risk_rank"] as! Int == 2)
        {cell.Risk_rank_header.text = "LOW" }
            
        else if(recall["risk_rank"] as! Int == 3)
        {cell.Risk_rank_header.text = "MEDIUM" }
            
        else if(recall["risk_rank"] as! Int == 4)
        {cell.Risk_rank_header.text = "HIGH" }
            
        else if(recall["risk_rank"] as! Int == 5)
        {cell.Risk_rank_header.text = "EXTREME" }
        
        //name
        if (recall["name"] as? String != nil){
            cell.recall_name.text = recall["name"] as? String
        }
        else{
            cell.recall_name.text = "N/A"
        }
        //nhtsa id
        if(recall["nhtsa_id"] as? String != nil){
            cell.NHTSA_Code.text = recall["nhtsa_id"] as? String
        }else{
            cell.NHTSA_Code.text = "N/A"
        }
        //oem id
        if(recall["oem_id"] as? String != nil){
            cell.OEM_Code.text = recall["oem_id"] as? String
        }else{
            cell.OEM_Code.text = "N/A"
        }
        //overall rank
        cell.Overall_score.text = String ( recall["overall_rank"] as! Int ) + " " + "out of" + " " + String ( recall["labor_difficulty"] as! Int )
        //risk rank
        cell.Risk_score.text = String (recall["risk_rank"] as! Int) + " " + "out of" + " " + String ( recall["labor_difficulty"] as! Int )
        //profit rank
        cell.Profit_score.text = String ( recall["profit_rank"] as! Int ) + " " + "out of" + " " + String ( recall["labor_difficulty"] as! Int )
        //age
        if(recall["recall_age"] as? Int != nil)
        { cell.Age.text = String (recall["recall_age"] as! Int)
        }else
        {cell.Age.text = "N/A"}
        
        //description
        if (recall["description"] as? String != nil){
            cell.Discription.text = recall["description"] as? String}
        else{
            cell.Discription.text = "N/A"
        }
        //risk
        if(recall["risk"] as? String != nil){cell.Risk.text = recall["risk"] as? String}
        else{cell.Risk.text = "N/A"}
        //risk type
        if(recall["risk_type"] as? String != nil){cell.Risk_type.text = recall["risk_type"] as? String}
        else{cell.Risk_type.text = "N/A"}
        //remedy
        if(recall["remedy"] as? String != nil){cell.Remedy.text = recall["remedy"] as? String}
        else{cell.Remedy.text = "N/A"}
        //is_remedy_available
        
        
        
        if(recall["is_remedy_available"] as? Int == 1)
            
        {
            cell.Remedy_Available.text = "Yes"
            cell.remedyBGView.backgroundColor = UIColor(red: 199.0/255, green: 239.0/255, blue: 245.0/255, alpha: 1)
        }
        else{
            cell.Remedy_Available.text = "No"
            cell.remedyBGView.backgroundColor = UIColor(red: 245.0/255, green: 193.0/255, blue: 198.0/255, alpha: 1)
        }
        //let str : String = recall["are_parts_available"]
        // print(str)
        //are_parts_available
        if(recall["are_parts_available"] as? Int == 1)
        {
            cell.partsAvailableBGview.backgroundColor = UIColor(red: 199.0/255, green: 239.0/255, blue: 245.0/255, alpha: 1)
            cell.Parts_Available.text = "Yes"}
        else
        {
            cell.partsAvailableBGview.backgroundColor = UIColor(red: 245.0/255, green: 193.0/255, blue: 198.0/255, alpha: 1)
            cell.Parts_Available.text = "No"}
        
        //parts available date
        if (recall["parts_available_date"] as? String != nil){cell.Parts_Available_Date.text = recall["parts_available_date"] as? String}
        else{cell.Parts_Available_Date.text = "N/A"}
        //effective date
        if(recall["effective_date"] as? String != nil){cell.Effective_Date.text = recall["effective_date"] as? String}
        else{cell.Effective_Date.text = "N/A"}
        //expiration date
        if(recall["expiration_date"] as? String != nil){cell.Expiration_Date.text = recall["expiration_date"] as? String}
        else{cell.Expiration_Date.text = "N/A"}
        //labor max
        if(recall["labor_max"] as? String != nil)
        {
            let d = Double (recall["labor_max"] as! String)
            let t =  self.hourToString(d!)
            cell.Labor_Max.text = t}
        else{ cell.Labor_Max.text = "N/A"}
        //labor min
        if(recall["labor_min"] as? String != nil)
        {
            
            let x = Double (recall["labor_min"] as! String)
            let y = self.hourToString(x!)
            cell.Labor_Min.text = y }
        else
        {cell.Labor_Min.text = "N/A"}
        //labour difficulti
        if(recall["labor_difficulty"] as! Int == 1)
        {cell.Level_Of_Difficulty.text = "Sticker" }
            
        else if(recall["labor_difficulty"] as! Int == 2)
        {cell.Level_Of_Difficulty.text = "Nuisance" }
            
        else if(recall["labor_difficulty"] as! Int == 3)
        {cell.Level_Of_Difficulty.text = "Programing Only" }
            
        else if(recall["labor_difficulty"] as! Int == 4)
        {cell.Level_Of_Difficulty.text = "No car Lift" }
            
        else if(recall["labor_difficulty"] as! Int == 5)
        {cell.Level_Of_Difficulty.text = "Car Lift" }
        
        // re
        if(recall["reimbursement"] as? Double != nil)
        { cell.Reimbursement_Amount.text = "$" + String (recall["reimbursement"] as! Double)}
        else
        { cell.Reimbursement_Amount.text = "N/A"}
        
        //stop sale
        if(recall["stop_sale"] as? Int == 1)
        {
            cell.stopSaleBG.backgroundColor = UIColor(red: 199.0/255, green: 239.0/255, blue: 245.0/255, alpha: 1)
            cell.Stop_Sale.text = "Yes"
        }
        else
        {
            cell.stopSaleBG.backgroundColor = UIColor(red: 245.0/255, green: 193.0/255, blue: 198.0/255, alpha: 1)
            cell.Stop_Sale.text = "No"
        }
        //dont drive
        if(recall["dont_drive"] as? Int == 1)
        {
            cell.dontDriveBG.backgroundColor = UIColor(red: 199.0/255, green: 239.0/255, blue: 245.0/255, alpha: 1)
            cell.Dont_Drive.text = "Yes"
        }
        else{
            cell.dontDriveBG.backgroundColor = UIColor(red: 245.0/255, green: 193.0/255, blue: 198.0/255, alpha: 1)
            cell.Dont_Drive.text =  "No"
        }
        
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if((indexPath as NSIndexPath).row == 0) {
            
            return 270
        }
        else {
            
            return 1000
            
        }
    }
    
    
    
    @IBAction func popUpEmailAction(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            self.popUp.imgEmail.image = UIImage.init(named: "radioFill")
        }
        else{
            sender.tag = 0
            self.popUp.imgEmail.image = UIImage.init(named: "radioEmpty")
        }
    }

    @IBAction func popUpSMSAction(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            self.popUp.imgText.image = UIImage.init(named: "radioFill")
        }
        else{
            sender.tag = 0
            self.popUp.imgText.image = UIImage.init(named: "radioEmpty")
        }
    }
    
    @IBAction func popupCancelAction(_ sender: UIButton) {
        self.dismissPopup()
    }
    
    
    
    @IBAction func popupSubscribeAction(_ sender: UIButton) {
        
        
        if self.capturedCode == "" {
            SVProgressHUD.dismiss()
            SCLAlertView().showError("", subTitle:"Please enter a VIN")
        }
        else if self.userEmail == ""{
            
            SVProgressHUD.dismiss()
            SCLAlertView().showError("", subTitle:"Please update your Email to receive email Alerts")
        }
        else if self.userPhone == ""{
            
            SVProgressHUD.dismiss()
            SCLAlertView().showError("", subTitle:"Please update your Phone to receive Mobile Alerts")
        }
            
        else
        {
            //let parameters = ["vin": "\(self.capturedCode)"] as [String : Any]
            //self.subscribeToAlertsWithVIN(method: .post, parameter: parameters)
            
            if self.popUp.btnText.tag == 1 || self.popUp.btnEmail.tag == 1  {
                if self.isalreadySubscribed == true {
                    let isAlertActive : Bool = self.foundDict["is_alert"] as! Bool
                    if isAlertActive == true {
                        let subscriptionId : Int = self.foundDict["id"] as! Int
                        if subscriptionId != 0 {
                            alertBtn.setBackgroundImage(UIImage(named: "uncheckBtn"), for: UIControlState.normal)
                            let parameters = ["is_alert": "False",
                                              "is_email" : "False",
                                              "is_sms" : "False"] as [String : Any]
                            self.changeSubscriptionToTrueOrFalse(method: .patch, parameter: parameters, vinId: subscriptionId)
                            print("unsubscribe")
                        }
                        else{
                            print("ISSUE")
                        }
                    }
                    else{
                        let subscriptionId : Int = self.foundDict["id"] as! Int
                        
                        var alertEmail : String = "False"
                        var alertSMS : String = "False"

                        if self.popUp.btnText.tag == 1 {
                            alertSMS = "True"
                        }
                        
                        if self.popUp.btnEmail.tag == 1 {
                            alertEmail = "True"
                        }
                        
                        if subscriptionId != 0 {
                            let parameters = ["is_alert": "True",
                                              "is_email" : alertEmail,
                                              "is_sms" : alertSMS] as [String : Any]
                            self.changeSubscriptionToTrueOrFalse(method: .patch, parameter: parameters, vinId: subscriptionId)
                            alertBtn.setBackgroundImage(UIImage(named: "checkBtn"), for: UIControlState.normal)
                            print("subscribe")
                        }
                        else{
                            print("ISSUE")
                        }
                    }
                }
                else{
                    
                }
                
                
                
                
                
                
                
               
                
            }
            else
            {
                
            }
        }
        
        
    }

    
    func dismissPopup() {
        if self.popup != nil {
            self.dismissPopupViewController(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func alertBtnTapped(_ sender: UIButton) {
        
        print("Phone = \(self.userPhone)")
        print("email = \(self.userEmail)")
        
        
        
        
        self.popUp = AlertOptions(nibName: "AlertOptions", bundle: nil)
       
        
        self.presentPopupViewController(self.popUp, animated: true, completion: {() -> Void in
            
            self.popUp.btnEmail.tag = 0
            self.popUp.imgEmail.image = UIImage.init(named: "radioEmpty")
            
            self.popUp.btnText.tag = 0
            self.popUp.imgText.image = UIImage.init(named: "radioEmpty")
            
            self.popUp.doneBtn.setTitle("SUBSCRIBE", for: .normal)
            if self.isalreadySubscribed == true {
                
                let isAlertActive : Bool = self.foundDict["is_alert"] as! Bool
                if isAlertActive == true {
                    let isEmailActive : Bool = self.foundDict["is_email"] as! Bool
                    if isEmailActive == true {
                        self.popUp.btnEmail.tag = 1
                        self.popUp.imgEmail.image = UIImage.init(named: "radioFill")
                    }
                    
                    let isSMSActive : Bool = self.foundDict["is_sms"] as! Bool
                    if isSMSActive == true {
                        self.popUp.btnText.tag = 1
                        self.popUp.imgText.image = UIImage.init(named: "radioFill")
                    }
                    self.popUp.doneBtn.setTitle("UNSUBSCRIBE", for: .normal)

                }
            }

            self.popUp.btnEmail.addTarget(self, action: #selector(self.popUpEmailAction) , for: UIControlEvents.touchUpInside)
            self.popUp.btnText.addTarget(self, action: #selector(self.popUpSMSAction) , for: UIControlEvents.touchUpInside)
            
            self.popUp.doneBtn.addTarget(self, action: #selector(self.popupSubscribeAction) , for: UIControlEvents.touchUpInside)
            self.popUp.btnCancel.addTarget(self, action: #selector(self.popupCancelAction) , for: UIControlEvents.touchUpInside)

        })
        
        
        return
        
        
      }
    func subscribeToAlertsWithVIN (method : HTTPMethod , parameter : Parameters) {
        
        
        SVProgressHUD.show(withStatus: "")
        let StoredToken = UserDefaults.standard
        let  auth_token = StoredToken.string(forKey: "auth_token")
        
        print("Token" + " " + auth_token!)
        //http headers
        let headers: HTTPHeaders = [
            "Authorization": "Token" + " " + auth_token!,
            "Accept": "application/json"
        ]
        //sending request
        Alamofire.request("https://app.recallmasters.com/api/v1/subscriptions/" , method: method , parameters : parameter , headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success(let JSON):
                SVProgressHUD.dismiss()
                print("Success with JSON: \(JSON)")
                print(response)
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss()
                
                SCLAlertView().showError("", subTitle:error.localizedDescription)
                print("Request failed with error: \(error.localizedDescription)")
                break
            }
        }
    }

    func changeSubscriptionToTrueOrFalse (method : HTTPMethod , parameter : Parameters ,vinId : Int) {
        
        
        SVProgressHUD.show(withStatus: "")
        let StoredToken = UserDefaults.standard
        let  auth_token = StoredToken.string(forKey: "auth_token")
        
        //http headers
        let headers: HTTPHeaders = [
            "Authorization": "Token" + " " + auth_token!,
            "Accept": "application/json"
        ]
        //sending request
        Alamofire.request("https://app.recallmasters.com/api/v1/subscriptions/\(vinId)/" , method: method , parameters : parameter , headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success(let JSON):
                SVProgressHUD.dismiss()
                print("Success with JSON: \(JSON)")
                print(response)
                
                if let json = response.result.value as? [String: Any] {
                    self.isalreadySubscribed = true
                    self.foundDict = json
                    self.dismissPopup()
                }
                
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss()
                
                SCLAlertView().showError("", subTitle:error.localizedDescription)
                print("Request failed with error: \(error.localizedDescription)")
                break
            }
        }
    }
    
    func getProfileData (method : HTTPMethod , parameter : Parameters)
    {
        SVProgressHUD.show(withStatus: "")
        let StoredToken = UserDefaults.standard
        let  auth_token = StoredToken.string(forKey: "auth_token")
        
        //http headers
        let headers: HTTPHeaders = [
            "Authorization": "Token" + " " + auth_token!,
            "Accept": "application/json"
        ]
        //sending request
        Alamofire.request("https://app.recallmasters.com/api/v1/auth/me/" , method: method , parameters : parameter , headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success(let JSON):
                SVProgressHUD.dismiss()
                print("Success with JSON: \(JSON)")
                //setting text in fields
                let response = JSON as! NSDictionary
                
                self.userEmail =  (response.object(forKey: "email")! as? String)!
                 self.userPhone =  (response.object(forKey: "phone")! as? String)!
               
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss()
                
                SCLAlertView().showError("", subTitle:error.localizedDescription)
                print("Request failed with error: \(error.localizedDescription)")
                break
            }
        }
        
    }

    // MARK: - actions
    @IBAction func scanAnotherVinAction(_ sender: AnyObject) {
        self.popUpView.isHidden = true
        self.performSegue(withIdentifier: "recallChecktoLoginBack", sender: nil)
        
        
        //        self.controller.reset()
        //        self.present(controller, animated: true, completion: nil)
        
    }
    
    @IBAction func recalCheckAction(_ sender: AnyObject) {
        
        self.isalreadySubscribed = false
        SVProgressHUD .show(withStatus: "Loading...")
        self.getRequest()
    }
    
    @IBAction func historyAction(_ sender: AnyObject) {
        self.popUpView.isHidden = true
        performSegue(withIdentifier: "recallToHistory", sender: self)
    }
    
    @IBAction func helpAction(_ sender: AnyObject) {
        self.popUpView.isHidden = true
        performSegue(withIdentifier: "recalToHelp", sender: self)
    }
    
    @IBAction func loginAction(_ sender: AnyObject) {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey: "IsFirstTimeLogged")
        self.popUpView.isHidden = true
        
        let StoredToken = UserDefaults.standard
        StoredToken.setValue(nil, forKey: "txtUsername")
        StoredToken.setValue(nil, forKey: "txtPassword")
        StoredToken.synchronize()
        self.performSegue(withIdentifier: "111", sender: nil)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "recallCheck-profile" {
            let viewController:ProfileViewController = segue.destination as! ProfileViewController
            
            viewController.From = "EnterVin"
            
        }
    }
    
    @IBAction func openProfileAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "recallCheck-profile", sender: nil)
    }
    @IBAction func openPopUpAction(_ sender: AnyObject) {
        if(self.popUpView.isHidden == true) {
            self.popUpView.isHidden=false
        }else{
            
            self.popUpView.isHidden=true
        }
        
    }
    @IBAction func BackAction(_ sender: AnyObject) {
        if isFrom == "home" {
            self.performSegue(withIdentifier: "enterVinToHome", sender: nil)
        }
        else
        {
            self.performSegue(withIdentifier: "recallChecktoLoginBack", sender: nil)
        }
    }
    @IBAction func takeTourAction(_ sender: UIButton) {
        createAndShowTheIntroView()
    }
    // MARK: - methods
    func createAndShowTheIntroView() {
        
        let page1 = EAIntroPage()
        page1.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page1.titleIconView = UIImageView(image: UIImage(named: "imgCamera")!)
        page1.titleIconView.frame = self.frameView.frame
        page1.title = "Vin Scan Page"
        page1.desc = ""
        
        let page2 = EAIntroPage()
        page2.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page2.titleIconView = UIImageView(image: UIImage(named: "imgRecallCheck-1")!)
        page2.titleIconView.frame = self.frameView.frame
        page2.title = "You can manually scan a VIN"
        page2.desc = ""
        
        let page3 = EAIntroPage()
        page3.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page3.titleIconView = UIImageView(image: UIImage(named: "imgHistory-1")!)
        page3.titleIconView.frame = self.frameView.frame
        page3.title = "History Screen"
        page3.desc = ""
        
        let page4 = EAIntroPage()
        page4.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page4.titleIconView = UIImageView(image: UIImage(named: "alertScreen-1")!)
        page4.titleIconView.frame = self.frameView.frame
        page4.title = ""
        page4.desc = ""
        
        let page8 = EAIntroPage()
        page8.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page8.titleIconView = UIImageView(image: UIImage(named: "cellSwipeScreen-1")!)
        page8.titleIconView.frame = self.frameView.frame
        page8.title = ""
        page8.desc = ""
        
        
        let intro = EAIntroView(frame: self.view.bounds, andPages: [page1, page2 , page3 , page4 ,page8])
        intro?.delegate = self
        intro?.show(in: self.view, animateDuration: 0.0)
    }
    
    func hourToString(_ hour:Double) -> String {
        let hours = Int(floor(hour))
        let mins = Int(floor(hour * 60).truncatingRemainder(dividingBy: 60))
        let secs = Int(floor(hour * 3600).truncatingRemainder(dividingBy: 60))
        
        print(hours)
        
        if hours > 1 {
            return String(format:"1 Day")
        }
        return String(format:"%dh %02d min", hours, mins, secs)
    }
    
    @IBAction func unwindToRecallCheck(_ segue: UIStoryboardSegue) {
    }
    func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        //self.popup.hidden = true
    }
}

extension RecallChekViewController: BarcodeScannerCodeDelegate,BarcodeScannerErrorDelegate,BarcodeScannerDismissalDelegate {
    
    func barcodeScanner(_ controller: BarcodeScannerController, didCaptureCode code: String, type: String) {
        
        
        self.capturedCode = code
        if(self.capturedCode[self.capturedCode.startIndex] == "I"){
            
            self.capturedCode.remove(at: self.capturedCode.startIndex)
            
        }
        
        self.customClass.VinNo = self.capturedCode
        //self.popup.hidden = true
        
        controller.dismiss(animated: true, completion: {
            SVProgressHUD .show()
            self.getRequest()
        })
    }
    
    func barcodeScanner(_ controller: BarcodeScannerController, didCapturedCode code: String){
        //print("Captured number is \(code)")
    }
    
    
    /// Delegate to report errors.
    func barcodeScanner(_ controller: BarcodeScannerController, didReceiveError error: Error){
        
    }
    
    /// Delegate to dismiss barcode scanner when the close button has been pressed.
    func barcodeScannerDidDismiss(_ controller: BarcodeScannerController){
        
        
        
    }
}
extension Date
{
    func hour() -> Int
    {
        //Get Hour
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components(.hour, from: self)
        let hour = components.hour
        
        //Return Hour
        return hour!
    }
    
    
    func minute() -> Int
    {
        //Get Minute
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components(.minute, from: self)
        let minute = components.minute
        
        //Return Minute
        return minute!
    }
    
    func toShortTimeString() -> String
    {
        //Get Short Time String
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        let timeString = formatter.string(from: self)
        
        //Return Short Time String
        return timeString
    }
}
