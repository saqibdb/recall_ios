//
//  ForgetPopUp.swift
//  Recall Check
//
//  Created by ibuildx on 11/10/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit
import CWPopup

class ForgetPopUp: UIViewController {

    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var sendBTN: UIButton!
    @IBOutlet weak var cancelBTN: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
