//
//  RecallTableViewCell.swift
//  RecallMasters VIN Check
//
//  Created by ibuildx on 9/23/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit

class RecallTableViewCell: UITableViewCell {
    
    
    
    
    @IBOutlet weak var labourMinMaxTopSpace: NSLayoutConstraint!
    @IBOutlet weak var profitScoreAndAgeTopSpace: NSLayoutConstraint!
    //outlets
    
    @IBOutlet weak var stopSaleBG: UIView!
    
    @IBOutlet weak var dontDriveBG: UIView!
    @IBOutlet weak var partsAvailableBGview: UIView!
    @IBOutlet weak var remedyBGView: UIView!
    @IBOutlet weak var Risk_rank_header: UILabel!
    @IBOutlet weak var recall_name: UILabel!
    @IBOutlet weak var NHTSA_Code: UILabel!
    @IBOutlet weak var OEM_Code: UILabel!
    @IBOutlet weak var Overall_score: UILabel!
    @IBOutlet weak var Risk_score: UILabel!
    @IBOutlet weak var Profit_score: UILabel!
    @IBOutlet weak var Age: UILabel!
    @IBOutlet weak var Discription: UILabel!
    @IBOutlet weak var Risk: UILabel!
    @IBOutlet weak var Remedy: UILabel!
    @IBOutlet weak var Remedy_Available: UILabel!
    @IBOutlet weak var Parts_Available: UILabel!
    @IBOutlet weak var Parts_Available_Date: UILabel!
    @IBOutlet weak var Effective_Date: UILabel!
    @IBOutlet weak var Expiration_Date: UILabel!
    @IBOutlet weak var Labor_Min: UILabel!
    @IBOutlet weak var Labor_Max: UILabel!
    @IBOutlet weak var Level_Of_Difficulty: UILabel!
    @IBOutlet weak var Reimbursement_Amount: UILabel!
    @IBOutlet weak var Stop_Sale: UILabel!
    @IBOutlet weak var Dont_Drive: UILabel!
    @IBOutlet weak var Risk_type: UILabel!
    override func awakeFromNib() {
    super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
