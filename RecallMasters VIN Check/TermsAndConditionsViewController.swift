//
//  TermsAndConditionsViewController.swift
//  Recall Check
//
//  Created by ibuildx on 11/10/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController {
    var isFrom : String = ""
    
    
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .default

        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        
        self.textView.contentOffset = CGPoint.zero;
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backAction(_ sender: UIButton) {
        if isFrom == "profile" {
            self.performSegue(withIdentifier: "termsToProfile", sender: nil)
        
        }
        else if isFrom == "businessVC" {
            self.performSegue(withIdentifier: "backToBusinessAction", sender: nil)
        }
        else {
            self.performSegue(withIdentifier: "backFromTermsAndconditions", sender: nil)
        }
    }
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
