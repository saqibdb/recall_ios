//
//  ViewController.swift
//  RecallMasters VIN Check
//
//  Created by iBuildx_Mac_Mini on 8/11/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//


import UIKit
import SVProgressHUD
import AMSmoothAlert
import SCLAlertView
import Alamofire
class ViewController: UIViewController  {
    
    
    var popUpView = ForgetPopUp()
    let controller = BarcodeScannerController()
    var titleString : String = ""
    var capturedCode : String = ""
    
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var createAccountBTN: UIButton!
    @IBOutlet weak var signInBTN: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        controller.codeDelegate = self
        controller.errorDelegate = self
        controller.dismissalDelegate = self
        
        let StoredToken = UserDefaults.standard
        if let userName : String = StoredToken.object(forKey: "txtUsername") as? String{
            if let userPass  : String = StoredToken.object(forKey: "txtPassword") as? String{
                self.post(["username":userName , "password":userPass], url: "https://app.recallmasters.com//api/v1/auth/login/")
            }
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tryItAction(_ sender: AnyObject) {
        
        let StoredToken = UserDefaults.standard
        StoredToken.removeObject(forKey: "auth_token")
        self.controller.reset()
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func signMeInAction(_ sender: AnyObject) {
        SVProgressHUD .show(withStatus: "Trying to login")
        
        if(self.txtUsername.text == "")
        {
            SVProgressHUD.dismiss()
            SCLAlertView().showError("Username", subTitle: "This field may not be blank")
        }
        else
        {
            self.post(["username":txtUsername.text! , "password":self.txtPassword.text!], url: "https://app.recallmasters.com//api/v1/auth/login/")
        }
    }
    func post(_ params : Dictionary<String, String>, url : String) {
        let request = NSMutableURLRequest(url: URL(string: url)! as URL)
        let session = URLSession.shared
        request.httpMethod = "POST"
        
        print(params)
        
        do
        {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        }
        catch {
            //handle error. Probably return or mark function as throws
            print(error)
            return
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            // handle error
            
            guard error == nil else {
                SVProgressHUD.dismiss()
                //let err = error?.description
                OperationQueue.main.addOperation {
                    SCLAlertView().showError("Sorry", subTitle:"The internet connection appears to be offline!")
                }
                return
            }
            
            let json: NSDictionary?
            do
            {
                json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? NSDictionary
            }
            catch let dataError {
                // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                print(dataError)
                
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error could not parse JSON: '\(jsonStr)'")
                print(jsonStr!)
                // return or throw?
                return
            }
            
            print(json!)
            if (json!["auth_token"] != nil)  {
                
                //SVProgressHUD.dismiss()
                print(self.txtUsername.text!)
                print(self.txtPassword.text!)
                print(params["username"]!)
                print(params["password"]!)

                let StoredToken = UserDefaults.standard
                StoredToken.setValue(json!["auth_token"], forKey: "auth_token")
                StoredToken.setValue(params["username"]!, forKey: "txtUsername")
                StoredToken.setValue(params["password"]!, forKey: "txtPassword")
                StoredToken.synchronize()
                
                OperationQueue.main.addOperation {
                    self.capturedCode = ""
                    self.checkUserTypeAndLogin(method: .get, parameter:["":""])
                }
            }
            else {
                SVProgressHUD.dismiss()
                OperationQueue.main.addOperation {
                    SCLAlertView().showError("error", subTitle: "Unable to login with the provided credentials!")
                }
            }
        })
        
        task.resume()
    }
    
    func checkUserTypeAndLogin (method : HTTPMethod , parameter : Parameters)
    {
        SVProgressHUD.show(withStatus: "")
        let StoredToken = UserDefaults.standard
        let  auth_token = StoredToken.string(forKey: "auth_token")
        
        //http headers
        let headers: HTTPHeaders = [
            "Authorization": "Token" + " " + auth_token!,
            "Accept": "application/json"
        ]
        //sending request
        Alamofire.request("https://app.recallmasters.com/api/v1/auth/me/" , method: method , parameters : parameter , headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success(let JSON):
                SVProgressHUD.dismiss()
                print("Success with JSON: \(JSON)")
                //setting text in fields
                let response = JSON as! NSDictionary
                
               
                let rType : String = (response.object(forKey: "registration_type")! as? String)!
                print(rType)
                let userType = UserDefaults.standard
                
                if(rType == "MOBILE_PERSONAL")
                {
                    print("user is personal")
                    userType.setValue("MOBILE_PERSONAL", forKey: "userType")
                }
                else
                {
                    userType.setValue("Buisness", forKey: "userType")
                    print("user is buisness")
                }
                
                
                let trialTimeStartDate = UserDefaults.standard
                //old date
                var limitDate = trialTimeStartDate.object(forKey: "LimitReachedOnDate") as? NSDate
                
                if limitDate == nil {
                    let trialTimeStartDate = UserDefaults.standard
                    trialTimeStartDate.set(NSDate(), forKey: "LimitReachedOnDate")
                    trialTimeStartDate.synchronize()
                    limitDate = NSDate()
                }
                
                //new date
                let newDate = Date()
                //compare days
                let numOfDays = self.daysBetween(date1: limitDate as! Date, date2: newDate)
                
                OperationQueue.main.addOperation {
                    
                    
                    if numOfDays > 30{
                        
                       self.showTrialExpirePopUp()
                    }
                    else
                    {
                        self.isAppAlreadyLaunchedOnce()
                    }
                 }
               
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss()
                
                SCLAlertView().showError("", subTitle:error.localizedDescription)
                print("Request failed with error: \(error.localizedDescription)")
                break
            }
        }
        
    }
    func showTrialExpirePopUp ()
    {
        var popUp = TrialExpiresPopUp()
        popUp = TrialExpiresPopUp(nibName: "TrialExpires", bundle: nil)
        self.presentPopupViewController(popUp, animated: true, completion: {() -> Void in
            
            popUp.okBtn.addTarget(self, action: #selector(self.dismissPopup) , for: UIControlEvents.touchUpInside)
           
        })
        
    }
    func daysBetween(date1: Date, date2: Date) -> Int {
        let calendar = Calendar.current
        
        let date1 = calendar.startOfDay(for: date1)
        let date2 = calendar.startOfDay(for: date2)
        
        let components = calendar.dateComponents([Calendar.Component.day], from: date1, to: date2)
        
        return components.day ?? 0
    }
    func isAppAlreadyLaunchedOnce() {
       
        SVProgressHUD.dismiss()
        if (UserDefaults.standard.bool(forKey: "IsFirstTimeLogged") == true)
        {
            self.performSegue(withIdentifier: "loginToInstructionPage", sender: nil)
        }
        else
        {
            //self.performSegue(withIdentifier: "loginToEnterVin", sender: nil)
            self.performSegue(withIdentifier: "loginToHome", sender: nil)
        }
    }
}

extension ViewController: BarcodeScannerCodeDelegate,BarcodeScannerErrorDelegate,BarcodeScannerDismissalDelegate {
    
    func barcodeScanner(_ controller: BarcodeScannerController, didCaptureCode code: String, type: String) {
        
        self.capturedCode = code
        controller.reset()
        self.dismiss(animated: true, completion: {self.performSegue(withIdentifier: "loginToEnterVin", sender: nil)})
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loginToEnterVin" {
            let viewController:RecallChekViewController = segue.destination as! RecallChekViewController
            
            viewController.capturedCode = self.capturedCode
        }
    }
    /// Delegate to handle the captured code.
    
    func barcodeScanner(_ controller: BarcodeScannerController, didCapturedCode code: String){
        print("Captured number is \(code)")
    }
    
    
    /// Delegate to report errors.
    func barcodeScanner(_ controller: BarcodeScannerController, didReceiveError error: Error){
        
    }
    
    /// Delegate to dismiss barcode scanner when the close button has been pressed.
    func barcodeScannerDidDismiss(_ controller: BarcodeScannerController){
        
        dismiss(animated: true, completion: nil)
        
    }
    @IBAction func unwindToLoginScreen(_ segue: UIStoryboardSegue) {
        dismiss(animated: true, completion: {
            self.controller.reset()

            self.present(self.controller, animated: true, completion: nil)})
    }
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    @IBAction func unwindToLoginScreen2(_ segue: UIStoryboardSegue) {
        SVProgressHUD.dismiss()
    }
    func openRoutesPopUp() {
        
        popUpView = ForgetPopUp(nibName: "ForgetPassword", bundle: nil)
        self.presentPopupViewController(popUpView, animated: true, completion: {() -> Void in
            
            self.popUpView.cancelBTN.addTarget(self, action: #selector(self.dismissPopup) , for: UIControlEvents.touchUpInside)
            self.popUpView.sendBTN.addTarget(self, action: #selector(self.popUpDonePressed) , for: UIControlEvents.touchUpInside)
        })
    }
    func dismissPopup() {
        if self.popup != nil {
            self.dismissPopupViewController(animated: true, completion: nil)
        }
    }
    @IBAction func forgetPasswordAction(_ sender: UIButton) {
        self.openRoutesPopUp()
    }
    func popUpDonePressed(){
    
        if popUpView.txtEmail.text == "" {
            print("email not specified")
        }
        else{
            print(popUpView.txtEmail.text)
            let parameters = ["email": "\(popUpView.txtEmail.text!)"] as [String : Any]
            self.ResetPasswordWithEmail(method: .post, parameter: parameters)
        
        }
    
    }
    func ResetPasswordWithEmail (method : HTTPMethod , parameter : Parameters) {
        
        
        SVProgressHUD.show(withStatus: "")
        
        
        //http headers
        let headers: HTTPHeaders = [
            
            "Accept": "application/json"
        ]
        //sending request
        Alamofire.request("https://app.recallmasters.com/api/v1/auth/password/reset/" , method: method , parameters : parameter , headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success(let JSON):
                SVProgressHUD.dismiss()
                print("Success with JSON: \(JSON)")
                //setting text in fields
                self.dismissPopup()
                
                print(response)
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss()
                
                SCLAlertView().showError("", subTitle:error.localizedDescription)
                print("Request failed with error: \(error.localizedDescription)")
                break
            }
        }
    }
   }

