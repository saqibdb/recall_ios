//
//  HomeScreenViewController.swift
//  Recall Check
//
//  Created by iBuildx-Mac3 on 2/23/17.
//  Copyright © 2017 iBuildX. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import SVProgressHUD
import RMessage
import EAIntroView

class HomeScreenViewController: UIViewController  ,EAIntroDelegate {
    var allVehicles : NSMutableArray = NSMutableArray()
    var  HistoryArray : NSArray = NSArray()
    
   var myTimer: Timer!
    @IBOutlet var homeButtons: [UIButton]!
    
    @IBOutlet weak var recallMonitorBtn: UIButton!
    @IBOutlet weak var frameView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        for btn : UIButton in homeButtons
        {
            btn.layer.cornerRadius = 6
        }
        HistoryArray  =  NSMutableArray(array: Vins.getAllVins())
        print(HistoryArray)

         NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("handleRecallMonitoring"), object: nil)
        
        if UserDefaults.standard.bool(forKey: "isMonitoring")
        {
           
            myTimer =    Timer.scheduledTimer(timeInterval: 120.0, target: self, selector: #selector(self.MonitorRecalls), userInfo: nil, repeats: true)
           
           self.recallMonitorBtn.setTitle("Monitoring Recalls", for: UIControlState.normal)
        }
        else{
          self.recallMonitorBtn.setTitle("Monitor Recalls", for: UIControlState.normal)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func HomeScreenActions(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            //scan a vin
            self.performSegue(withIdentifier: "hometologinBack", sender: nil)
            break
        case 1:
            // enter a vin
            self.performSegue(withIdentifier: "HomeToEnterVin", sender: nil)
            break
        case 2:
            //profile
            self.performSegue(withIdentifier: "homeToprofile", sender: nil)
            break
        case 3:
            //history
            self.performSegue(withIdentifier: "homeToHistory", sender: nil)
            break
        case 4:
            //take A tour
            self.createAndShowTheIntroView()
            break
        case 5:
            //help
            self.performSegue(withIdentifier: "homeToHelp", sender: nil)
            break
        case 6:
            //logout
            let prefs = UserDefaults.standard
            prefs.removeObject(forKey: "IsFirstTimeLogged")
            let StoredToken = UserDefaults.standard
            StoredToken.setValue(nil, forKey: "txtUsername")
            StoredToken.setValue(nil, forKey: "txtPassword")
            StoredToken.synchronize()
            
            self.performSegue(withIdentifier: "backToLoginScreen", sender: nil)
            break
        default:
            break
        }
      }
    
    func createAndShowTheIntroView() {
        
        
        let page1 = EAIntroPage()
        page1.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page1.titleIconView = UIImageView(image: UIImage(named: "imgCamera")!)
        page1.titleIconView.frame = self.frameView.frame
        page1.title = "Vin Scan Page"
        page1.desc = ""
        
        let page2 = EAIntroPage()
        page2.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page2.titleIconView = UIImageView(image: UIImage(named: "imgRecallCheck-1")!)
        page2.titleIconView.frame = self.frameView.frame
        page2.title = "You can manually scan a VIN"
        page2.desc = ""
        
        let page3 = EAIntroPage()
        page3.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page3.titleIconView = UIImageView(image: UIImage(named: "imgHistory-1")!)
        page3.titleIconView.frame = self.frameView.frame
        page3.title = "History Screen"
        page3.desc = ""
        
        let page4 = EAIntroPage()
        page4.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page4.titleIconView = UIImageView(image: UIImage(named: "alertScreen-1")!)
        page4.titleIconView.frame = self.frameView.frame
        page4.title = ""
        page4.desc = ""
        
        let page8 = EAIntroPage()
        page8.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page8.titleIconView = UIImageView(image: UIImage(named: "cellSwipeScreen-1")!)
        page8.titleIconView.frame = self.frameView.frame
        page8.title = ""
        page8.desc = ""
        
        let intro = EAIntroView(frame: self.view.bounds, andPages: [page1, page2 , page3 , page4 , page8])
        intro?.delegate = self
        intro?.show(in: self.view, animateDuration: 0.0)
    }
    
    @IBAction func unwindToHomeScreen(_ segue: UIStoryboardSegue) {
    }
    
    // MARK: - Orientation
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HomeToEnterVin" {
            let viewController:RecallChekViewController = segue.destination as! RecallChekViewController
            
            viewController.isFrom = "home"
         }
        if segue.identifier == "homeToprofile" {
            let viewController:ProfileViewController = segue.destination as! ProfileViewController
            
            viewController.From = "home"
        }
        if segue.identifier == "homeToHistory" {
            let viewController:HistoryViewController = segue.destination as! HistoryViewController
            
            viewController.isFrom = "home"
         }
        
        if segue.identifier == "homeToHelp" {
            let viewController:HelpViewController = segue.destination as! HelpViewController
            
            viewController.isFrom = "home"
        }
      }
    func methodOfReceivedNotification(notification: Notification){
        if UserDefaults.standard.bool(forKey: "isMonitoring")
        {
            
            myTimer =    Timer.scheduledTimer(timeInterval: 120.0, target: self, selector: #selector(self.MonitorRecalls), userInfo: nil, repeats: true)
            UserDefaults.standard.set(true, forKey: "isMonitoring")
            self.recallMonitorBtn.setTitle("Monitoring Recalls", for: UIControlState.normal)

          
        }
        else{
            myTimer.invalidate()
            myTimer = nil
            UserDefaults.standard.set(false, forKey: "isMonitoring")
            self.recallMonitorBtn.setTitle("Monitor Recalls", for: UIControlState.normal)
        }
    }

   
    @IBAction func continousRecallSubscriptionAction(_ sender: UIButton) {
        
 
        if UserDefaults.standard.bool(forKey: "isMonitoring")
        {
            myTimer.invalidate()
            myTimer = nil
             UserDefaults.standard.set(false, forKey: "isMonitoring")
            self.recallMonitorBtn.setTitle("Monitor Recalls", for: UIControlState.normal)
        }
        else{
             myTimer =    Timer.scheduledTimer(timeInterval: 120.0, target: self, selector: #selector(self.MonitorRecalls), userInfo: nil, repeats: true)
            UserDefaults.standard.set(true, forKey: "isMonitoring")
            self.recallMonitorBtn.setTitle("Monitoring Recalls", for: UIControlState.normal)
        }

    }
    func MonitorRecalls() {
        print("executing timer ...")
       
        let StoredToken = UserDefaults.standard
        let  auth_token = StoredToken.string(forKey: "auth_token")
        
        //http headers
        let headers: HTTPHeaders = [
            "Authorization": "Token" + " " + auth_token!,
            "Accept": "application/json"
        ]
        //sending request
        Alamofire.request("https://app.recallmasters.com/api/v1/subscriptions/?is_alert=True" , method: .get ,  headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success(let JSON):
                
                SVProgressHUD.dismiss()
                print("Success with JSON: \(JSON)")
                let response = JSON as! NSDictionary
                
                
                let allRecalls  : NSArray = response.object(forKey: "results") as! NSArray
                for recall in allRecalls {
                    
                    let tempRecall = recall as! NSDictionary
                    let vehicle = tempRecall.value(forKey: "vehicle")
                    self.allVehicles.add(vehicle)
                }
                
                
                for vehicle in self.allVehicles{
                    
                    let tempvhcl = vehicle as! NSDictionary
                    let apiVin = tempvhcl.value(forKey: "vin")!
                    let apiVinRecallCount = tempvhcl.value(forKey: "recall_count")! as! Int
                    
                    
                    for historyVin  in self.HistoryArray {
                        
                       // print ((historyVin as! Vins).vinNo)
                       // print (apiVin)
                        
                        
                        let temp : Int = Int((historyVin as! Vins).recallCount)
                        print(temp)
                        
                        if((historyVin as! Vins).vinNo == (apiVin as! String)  && apiVinRecallCount > temp){
                         
                            RMessage.showNotification(withTitle: "New Recalls Found", subtitle: "", iconImage: nil, type: RMessageType.normal, customTypeName: nil, duration: 3.0, callback: nil, buttonTitle: nil, buttonCallback: nil, at: RMessagePosition.navBarOverlay, canBeDismissedByUser: true)
                         }
                    }
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss()
                SCLAlertView().showError("", subTitle:error.localizedDescription)
                print("Request failed with error: \(error.localizedDescription)")
                break
            }
        }
    }

    
    
}
