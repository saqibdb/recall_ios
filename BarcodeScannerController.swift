import UIKit
import AVFoundation
import SVProgressHUD
import EAIntroView


// MARK: - Delegates

/// Delegate to handle the captured code.
public protocol BarcodeScannerCodeDelegate: class {
  func barcodeScanner(_ controller: BarcodeScannerController, didCaptureCode code: String, type: String)
}

/// Delegate to report errors.
public protocol BarcodeScannerErrorDelegate: class {
  func barcodeScanner(_ controller: BarcodeScannerController, didReceiveError error: Error)
}

/// Delegate to dismiss barcode scanner when the close button has been pressed.
public protocol BarcodeScannerDismissalDelegate: class {
  func barcodeScannerDidDismiss(_ controller: BarcodeScannerController)
}

// MARK: - Controller

/**
 Barcode scanner controller with 4 sates:
 - Scanning mode
 - Processing animation
 - Unauthorized mode
 - Not found error message
 */

var menuView : UIScrollView = UIScrollView(frame: CGRect(x: 20, y: 20, width: 200, height: 210))



open class BarcodeScannerController: UIViewController , EAIntroDelegate {

    
    lazy var redLineCenterFull : UIView = {
        let redLineCenterFull = UIView()
        redLineCenterFull.backgroundColor = UIColor.red
        return redLineCenterFull
    }()
    lazy var redLineCentersmall : UIView = {
        let redLineCentersmall = UIView()
        redLineCentersmall.backgroundColor = UIColor.red
        return redLineCentersmall
    }()
    
    
    
  /// Video capture device.
  lazy var captureDevice: AVCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)

  /// Capture session.
  lazy var captureSession: AVCaptureSession = AVCaptureSession()

  /// Header view with title and close button.
  lazy var headerView: HeaderView = HeaderView()

  /// Information view with description label.
  lazy var infoView: InfoView = InfoView()

  /// Button to change torch mode.
  lazy var flashButton: UIButton = { [unowned self] in
    let button = UIButton(type: .custom)
    button.addTarget(self, action: #selector(flashButtonDidPress), for: .touchUpInside)

    return button
    }()

  /// Animated focus view.
  lazy var focusView: UIView = {
    let view = UIView()
    view.layer.borderColor = UIColor.white.cgColor
    view.layer.borderWidth = 2
    view.layer.cornerRadius = 5
    view.layer.shadowColor = UIColor.white.cgColor
    view.layer.shadowRadius = 10.0
    view.layer.shadowOpacity = 0.9
    view.layer.shadowOffset = CGSize.zero
    view.layer.masksToBounds = false

    return view
  }()

  /// Button that opens settings to allow camera usage.
  lazy var settingsButton: UIButton = { [unowned self] in
    let button = UIButton(type: .system)
    let title = NSAttributedString(string: SettingsButton.text,
      attributes: [
        NSFontAttributeName : SettingsButton.font,
        NSForegroundColorAttributeName : SettingsButton.color,
      ])

    button.setAttributedTitle(title, for: UIControlState())
    button.sizeToFit()
    button.addTarget(self, action: #selector(settingsButtonDidPress), for: .touchUpInside)

    return button
    }()

  /// Video preview layer.
  var videoPreviewLayer: AVCaptureVideoPreviewLayer?

  /// The current controller's status mode.
  var status: Status = Status(state: .scanning) {
    didSet {
      let duration = status.animated &&
        (status.state == .processing
          || oldValue.state == .processing
          || oldValue.state == .notFound
        ) ? 0.5 : 0.0

      guard status.state != .notFound else {
        infoView.status = status

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
          self.status = Status(state: .scanning)
        }

        return
      }

      let delayReset = oldValue.state == .processing || oldValue.state == .notFound

      if !delayReset {
        resetState()
      }

      UIView.animate(withDuration: duration,
        animations: {
          self.infoView.frame = self.infoFrame
          self.infoView.status = self.status
        },
        completion: { _ in
          if delayReset {
            self.resetState()
          }

          self.infoView.layer.removeAllAnimations()
          if self.status.state == .processing {
            self.infoView.animateLoading()
          }
      })
    }
  }

  /// The current torch mode on the capture device.
  var torchMode: TorchMode = .off {
    didSet {
      guard captureDevice.hasFlash else { return }

      do {
        try captureDevice.lockForConfiguration()
        captureDevice.torchMode = torchMode.captureTorchMode
        captureDevice.unlockForConfiguration()
      } catch {}

      flashButton.setImage(torchMode.image, for: UIControlState())
    }
  }

  /// Calculated frame for the info view.
  var infoFrame: CGRect {
    let height = status.state != .processing ? 75 : view.bounds.height
    return CGRect(x: 0, y: view.bounds.height - height,
      width: view.bounds.width, height: height)
  }

  /// When the flag is set to `true` controller returns a captured code
  /// and waits for the next reset action.
  public var isOneTimeSearch = true

  /// Delegate to handle the captured code.
  public weak var codeDelegate: BarcodeScannerCodeDelegate?

  /// Delegate to report errors.
  public weak var errorDelegate: BarcodeScannerErrorDelegate?

  /// Delegate to dismiss barcode scanner when the close button has been pressed.
  public weak var dismissalDelegate: BarcodeScannerDismissalDelegate?

  /// Flag to lock session from capturing.
  var locked = false

  // MARK: - Initialization

  deinit {
    NotificationCenter.default.removeObserver(self)
  }

  // MARK: - View lifecycle

  open override func viewDidLoad() {
    super.viewDidLoad()

    videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
    videoPreviewLayer?.videoGravity = AVLayerVideoGravityResize

    view.backgroundColor = UIColor.black

    guard let videoPreviewLayer = videoPreviewLayer else {
      return
    }

    view.layer.addSublayer(videoPreviewLayer)

    /*
    [infoView, headerView, settingsButton, flashButton, focusView].forEach {
      view.addSubview($0)
      view.bringSubview(toFront: $0)
    }

 */
    
    
    [infoView, headerView, settingsButton, flashButton, focusView, redLineCenterFull ,redLineCentersmall].forEach {
        view.addSubview($0)
        view.bringSubview(toFront: $0)
    }
    infoView.isHidden = true
    
    torchMode = .off
    focusView.isHidden = true
    headerView.delegate = self

    setupCamera()

    NotificationCenter.default.addObserver(
      self, selector: #selector(appWillEnterForeground),
      name: NSNotification.Name.UIApplicationWillEnterForeground,
      object: nil)
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(BarcodeScannerController.handleTap(sender:)))
    self.view.addGestureRecognizer(tap)
  }
    
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        
        menuView.isHidden = true//
    }
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //setupFrames()
        //infoView.setupFrames()
        
       // headerView.isHidden = !isBeingPresented
    }
    
  open override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //animateFocusView()
  }

  /**
   `UIApplicationWillEnterForegroundNotification` action.
   */
  func appWillEnterForeground() {
    torchMode = .off
    //animateFocusView()
  }

  // MARK: - Configuration

  /**
   Sets up camera and checks for camera permissions.
   */
  func setupCamera() {
    let authorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)

    if authorizationStatus == .authorized {
      setupSession()
      status = Status(state: .scanning)
    } else if authorizationStatus == .notDetermined {
      AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo,
        completionHandler: { (granted: Bool) -> Void in
          DispatchQueue.main.async {
            if granted {
              self.setupSession()
            }

            self.status = granted ? Status(state: .scanning) : Status(state: .unauthorized)
          }
      })
    } else {
      status = Status(state: .unauthorized)
    }
  }

  /**
   Sets up capture input, output and session.
   */
  func setupSession() {
    do {
      let input = try AVCaptureDeviceInput(device: captureDevice)
      captureSession.addInput(input)
    } catch {
      errorDelegate?.barcodeScanner(self, didReceiveError: error)
    }

    let output = AVCaptureMetadataOutput()
    captureSession.addOutput(output)
    output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
    output.metadataObjectTypes = metadata
    videoPreviewLayer?.session = captureSession

    view.setNeedsLayout()
  }

  // MARK: - Reset

  /**
   Shows error message and goes back to the scanning mode.

   - Parameter errorMessage: Error message that overrides the message from the config.
   */
  public func resetWithError(message: String? = nil) {
    status = Status(state: .notFound, text: message)
  }

  /**
   Resets the controller to the scanning mode.

   - Parameter animated: Flag to show scanner with or without animation.
   */
  public func reset(animated: Bool = true) {
    status = Status(state: .scanning, animated: animated)
  }

  /**
   Resets the current state.
   */
  func resetState() {
    let alpha: CGFloat = status.state == .scanning ? 1 : 0

    torchMode = .off
    locked = status.state == .processing && isOneTimeSearch

    status.state == .scanning
      ? captureSession.startRunning()
      : captureSession.stopRunning()

    focusView.alpha = alpha
    flashButton.alpha = alpha
    settingsButton.isHidden = status.state != .unauthorized
  }

  // MARK: - Layout
  open override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    

    menuView.contentSize = CGSize(width: 200, height: 410)

    
    flashButton.frame = CGRect(x: view.frame.height - 50, y: view.frame.width - 50, width: 37, height: 37)
    menuView.backgroundColor = UIColor(red: 250.0 / 255.0, green: 250.0 / 255.0, blue: 250.0 / 255.0, alpha: 1.0)
    
    let enterVinBtn : UIButton = UIButton(frame: CGRect(x: 20, y: 100, width: 200, height: 50))
    enterVinBtn.setTitle(NSLocalizedString("Enter VIN", comment: ""), for: .normal)
    enterVinBtn.setTitleColor(UIColor.black, for: .normal)
    enterVinBtn.titleLabel!.font =  UIFont.systemFont(ofSize: 17)
    enterVinBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
    enterVinBtn.addTarget(self, action:#selector(self.enterVinAction),for: .touchUpInside)
    
    let historyBtn : UIButton = UIButton(frame: CGRect(x: 20, y: 150, width: 200, height: 50))
    historyBtn.setTitle(NSLocalizedString("History", comment: ""), for: .normal)
    historyBtn.titleLabel?.font = UIFont.systemFont(ofSize: 17)
    historyBtn.setTitleColor(UIColor.black, for: .normal)
    historyBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
    historyBtn.addTarget(self, action:#selector(self.openHistory),for: .touchUpInside)
    
    let homeBtn : UIButton = UIButton(frame: CGRect(x: 20, y: 0, width: 200, height: 50))
    homeBtn.setTitle(NSLocalizedString("Home", comment: ""), for: .normal)
    homeBtn.titleLabel?.font = UIFont.systemFont(ofSize: 17)
    homeBtn.setTitleColor(UIColor.black, for: .normal)
    homeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
    homeBtn.addTarget(self, action:#selector(self.openHome),for: .touchUpInside)
    
    let takeATour : UIButton = UIButton(frame: CGRect(x: 20, y: 200, width: 200, height: 50))
    takeATour.setTitle(NSLocalizedString("Take a Tour", comment: ""), for: .normal)
    takeATour.titleLabel?.font = UIFont.systemFont(ofSize: 17)
    takeATour.setTitleColor(UIColor.black, for: .normal)
    takeATour.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
    takeATour.addTarget(self, action:#selector(self.createAndShowTheIntroView),for: .touchUpInside)
    
    let helpBtn : UIButton = UIButton(frame: CGRect(x: 20, y: 250, width: 200, height: 50))
    helpBtn.setTitle(NSLocalizedString("Help", comment: ""), for: .normal)
    helpBtn.titleLabel?.font = UIFont.systemFont(ofSize: 17)
    helpBtn.setTitleColor(UIColor.black, for: .normal)
    helpBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
    helpBtn.addTarget(self, action:#selector(self.openHelp),for: .touchUpInside)
    
    //new
    
    let ProfileBtn : UIButton = UIButton(frame: CGRect(x: 20, y: 50, width: 200, height: 50))
    ProfileBtn.setTitle(NSLocalizedString("Profile", comment: ""), for: .normal)
    ProfileBtn.titleLabel?.font = UIFont.systemFont(ofSize: 17)
    ProfileBtn.setTitleColor(UIColor.black, for: .normal)
    ProfileBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
    ProfileBtn.addTarget(self, action:#selector(self.openProfile),for: .touchUpInside)

    
    let loginBtn : UIButton = UIButton(frame: CGRect(x: 20, y: 300, width: 200, height: 50))
    loginBtn.setTitleColor(UIColor.black, for: .normal)
    loginBtn.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(17))
    loginBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
    loginBtn.addTarget(self, action:#selector(self.loginAction),for: .touchUpInside)
    
    let freeToken = "9931b14a2be3944f405a0f8f07eb739c17a00e41"
    let StoredToken = UserDefaults.standard
    
    if(( StoredToken.string(forKey: "auth_token")) != nil && ( StoredToken.string(forKey: "auth_token")) != freeToken){
        
        loginBtn.setTitle(NSLocalizedString("Log out", comment: ""), for:UIControlState())
    }
    else{
        StoredToken.setValue(freeToken, forKey: "auth_token")
        loginBtn.setTitle(NSLocalizedString("Log in", comment: ""), for:UIControlState())
    }
    
    menuView.addSubview(homeBtn)
    menuView.addSubview(ProfileBtn)
    menuView.addSubview(enterVinBtn)
    menuView.addSubview(historyBtn)
    menuView.addSubview(takeATour)
    menuView.addSubview(helpBtn)
    menuView.addSubview(loginBtn)

    
    self.view.addSubview(menuView)
    self.view.bringSubview(toFront: menuView)
    menuView.isHidden = true
    menuView.layer.cornerRadius = 2.0
   //menuView.clipsToBounds = true
    
    // headerView.button.frame = CGRect(x: view.frame.width - headerView.button.frame.width, y: 0, width: headerView.button.frame.width, height: headerView.button.frame.height)
    
    if videoPreviewLayer?.connection != nil {
        videoPreviewLayer?.connection.videoOrientation = .landscapeRight
    }
    infoView.frame = infoFrame
    if view.frame.width < view.frame.height {
        videoPreviewLayer?.frame = CGRect(x: 0, y: 0, width: view.frame.height, height: view.frame.width)
        
        redLineCenterFull.frame = CGRect(x: 0, y: (view.frame.width / 2) + 15, width: view.frame.height, height: 2)
        
        
        redLineCentersmall.frame = CGRect(x: (view.frame.height / 2), y: (view.frame.width / 2) - 3 , width: 2, height: 40)
        
        
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.height, height: 50)
    }
    else {
        
        videoPreviewLayer?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        redLineCenterFull.frame = CGRect(x: 0, y: (view.frame.height / 2) + 15, width: view.frame.width, height: 2)
        
        
        redLineCentersmall.frame = CGRect(x: (view.frame.width / 2), y: (view.frame.height / 2) - 3 , width: 2, height: 40)
        
        
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50)
    }
    

    
    
    
    /*

    headerView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 64)
    flashButton.frame = CGRect(x: view.frame.width - 50, y: 73, width: 37, height: 37)
    infoView.frame = infoFrame

    if let videoPreviewLayer = videoPreviewLayer {
      videoPreviewLayer.frame = view.layer.bounds
      if let connection = videoPreviewLayer.connection, connection.isVideoOrientationSupported {
        switch (UIApplication.shared.statusBarOrientation) {
        case .portrait: connection.videoOrientation = .portrait
        case .landscapeRight: connection.videoOrientation = .landscapeRight
        case .landscapeLeft: connection.videoOrientation = .landscapeLeft
        case .portraitUpsideDown: connection.videoOrientation = .portraitUpsideDown
        default: connection.videoOrientation = .portrait
        }
      }
    }

    center(subview: focusView, inSize: CGSize(width: 218, height: 150))
    center(subview: settingsButton, inSize: CGSize(width: 150, height: 50))

    headerView.isHidden = !isBeingPresented
 */
  }

  /**
   Sets a new size and center aligns subview's position.

   - Parameter subview: The subview.
   - Parameter size: A new size.
  */
  func center(subview: UIView, inSize size: CGSize) {
    subview.frame = CGRect(
      x: (view.frame.width - size.width) / 2,
      y: (view.frame.height - size.height) / 2,
      width: size.width,
      height: size.height)
  }

    
    // MARK: - Orientation
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    
    
    
  // MARK: - Animations

  /**
   Simulates flash animation.

   - Parameter processing: Flag to set the current state to `.Processing`.
   */
  func animateFlash(whenProcessing: Bool = false) {
    let flashView = UIView(frame: view.bounds)
    flashView.backgroundColor = UIColor.white
    flashView.alpha = 1

    view.addSubview(flashView)
    view.bringSubview(toFront: flashView)

    UIView.animate(withDuration: 0.2,
      animations: {
        flashView.alpha = 0.0
      },
      completion: { _ in
        flashView.removeFromSuperview()

        if whenProcessing {
          self.status = Status(state: .processing)
        }
    })
  }

  /**
   Performs focus view animation.
   */
  func animateFocusView() {
    focusView.layer.removeAllAnimations()
    focusView.isHidden = false

    UIView.animate(withDuration: 1.0, delay:0,
      options: [.repeat, .autoreverse, .beginFromCurrentState],
      animations: {
        self.center(subview: self.focusView, inSize: CGSize(width: 280, height: 80))
      }, completion: nil)

    view.setNeedsLayout()
  }

  // MARK: - Actions

  /**
   Opens setting to allow camera usage.
   */
  func settingsButtonDidPress() {
    DispatchQueue.main.async {
      if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
        UIApplication.shared.openURL(settingsURL)
      }
    }
  }

  /**
   Sets the next torch mode.
   */
  func flashButtonDidPress() {
    torchMode = torchMode.next
  }
    
    
    func loginAction () {
        
        let prefs = UserDefaults.standard
        menuView.isHidden = true
        
        prefs.removeObject(forKey: "IsFirstTimeLogged")
        let StoredToken = UserDefaults.standard
        StoredToken.setValue(nil, forKey: "txtUsername")
        StoredToken.setValue(nil, forKey: "txtPassword")
        StoredToken.synchronize()
        
        
        
        self.dismiss(animated: true, completion:  {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondVC = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            
            self.present(secondVC, animated: true, completion: nil)
            
        })
        
        //dismissalDelegate?.barcodeScannerDidDismiss(self)
    }
    
    func enterVinAction () {
        menuView.isHidden = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondVC = storyboard.instantiateViewController(withIdentifier: "recallCheckVc") as! RecallChekViewController
        
        self.present(secondVC, animated: true, completion: nil)
        
    }
    func openHelp () {
        menuView.isHidden = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondVC = storyboard.instantiateViewController(withIdentifier: "helpVc") as! HelpViewController
        
        present(secondVC, animated: true, completion: nil)
        
    }
    
    func openProfile () {
        menuView.isHidden = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondVC = storyboard.instantiateViewController(withIdentifier: "profileController") as! ProfileViewController
        
        present(secondVC, animated: true, completion: nil)
        
    }
    
    func openHistory () {
        menuView.isHidden = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondVC = storyboard.instantiateViewController(withIdentifier: "historyVc") as! HistoryViewController
        
        self.present(secondVC, animated: true, completion: nil)
        
        
    }
    func openHome () {
        menuView.isHidden = true
        menuView.removeFromSuperview()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondVC = storyboard.instantiateViewController(withIdentifier: "homeVc") as! HomeScreenViewController
        
        self.present(secondVC, animated: true, completion: nil)
        
    }
    
    @IBAction func unwindToBarcodeScannerView(_ segue: UIStoryboardSegue) {
    }
    
    
    func createAndShowTheIntroView() {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondVC = storyboard.instantiateViewController(withIdentifier: "introViewController") as! introViewController
        
        self.present(secondVC, animated: true, completion: nil)

        /*
        let frameView : UIView = UIView(frame: CGRect(x: 20, y: self.view.frame.size.width/2, width:self.view.frame.size.width/2 , height: self.view.frame.size.height/1.5))
        
        let page1 = EAIntroPage()
        page1.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page1.titleIconView = UIImageView(image: UIImage(named: "imgCamera")!)
        page1.titleIconView.contentMode = UIViewContentMode.scaleAspectFit
        page1.titleIconView.clipsToBounds = true
        page1.titleIconView.frame = frameView.frame
        page1.title = "Vin Scan Page"
        page1.desc = ""
        
        let page2 = EAIntroPage()
        page2.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page2.titleIconView = UIImageView(image: UIImage(named: "imgRecallCheck")!)
        page2.titleIconView.contentMode = UIViewContentMode.scaleAspectFit
        page2.titleIconView.clipsToBounds = true
        page2.titleIconView.frame = frameView.frame
        page2.title = "You can manually scan a VIN"
        page2.desc = ""
        
        let page3 = EAIntroPage()
        page3.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page3.titleIconView = UIImageView(image: UIImage(named: "imgHistory")!)
        page3.titleIconView.contentMode = UIViewContentMode.scaleAspectFit
        page3.titleIconView.clipsToBounds = true
        page3.titleIconView.frame = frameView.frame
        page3.title = "History Screen"
        page3.desc = ""
        
        let page4 = EAIntroPage()
        page4.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page4.titleIconView = UIImageView(image: UIImage(named: "alertScreen")!)
        page4.titleIconView.contentMode = UIViewContentMode.scaleAspectFit
        page4.titleIconView.clipsToBounds = true
        page4.titleIconView.frame = frameView.frame
        page4.title = ""
        page4.desc = ""
        
        
        let page8 = EAIntroPage()
        page8.bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        page8.titleIconView = UIImageView(image: UIImage(named: "cellSwipeScreen")!)
        page8.titleIconView.contentMode = UIViewContentMode.scaleAspectFit
        page8.titleIconView.clipsToBounds = true
        page8.titleIconView.frame = frameView.frame
        page8.title = ""
        page8.desc = ""
        
        let intro = EAIntroView(frame: self.view.bounds, andPages: [page1, page2 , page3 , page4 , page8])
        intro?.delegate = self
        intro?.show(in: self.view, animateDuration: 0.0)
       */
    }
}

// MARK: - AVCaptureMetadataOutputObjectsDelegate

extension BarcodeScannerController: AVCaptureMetadataOutputObjectsDelegate {

  public func captureOutput(_ captureOutput: AVCaptureOutput!,
                            didOutputMetadataObjects metadataObjects: [Any]!,
                            from connection: AVCaptureConnection!) {
    guard !locked else { return }
    guard metadataObjects != nil && !metadataObjects.isEmpty else { return }

    guard
      let metadataObj = metadataObjects[0] as? AVMetadataMachineReadableCodeObject,
      let code = metadataObj.stringValue,
      metadata.contains(metadataObj.type)
      else { return }

    if isOneTimeSearch {
      locked = true
    }

    animateFlash(whenProcessing: isOneTimeSearch)
    codeDelegate?.barcodeScanner(self, didCaptureCode: code, type: metadataObj.type)
  }
}

// MARK: - HeaderViewDelegate

extension BarcodeScannerController: HeaderViewDelegate {
    
    func headerViewDidPressClose(_ hederView: HeaderView) {
        
        //menuView.frame = CGRect(x: headerView.button.frame.origin.x - 150, y: 53, width: 200, height: 410)
        
        let screenRect: CGRect = UIScreen.main.bounds
        let screenWidth: CGFloat = screenRect.size.width
        let screenHeight: CGFloat = screenRect.size.height
        
        print(screenWidth)
        print(screenHeight)
        
        if (screenHeight == 320.0) {

            let point = CGPoint(x: headerView.button.frame.origin.x - 150, y: 53)
            menuView.frame.origin = point
            
        }
        else {
            let point = CGPoint(x: headerView.button.frame.origin.x - 150, y: 53)
            menuView.frame.origin = point
            
            let size = CGSize(width: 200, height: 360)
            menuView.frame.size = size
        }
        
        menuView.contentSize = CGSize(width: 200, height: 360)

        
        
        //let shadowPath = UIBezierPath(rect: menuView.bounds)
        //menuView.layer.masksToBounds = false
        //menuView.layer.shadowColor = UIColor.black.cgColor
        //menuView.layer.shadowOffset = CGSize(width: 0, height: 0)
        //menuView.layer.shadowOpacity = 0.5
        //menuView.layer.shadowPath = shadowPath.cgPath
        
        if menuView.isHidden == true {
            menuView.isHidden = false
            
        }
        else{
            menuView.isHidden = true
        }
        self.view.bringSubview(toFront: menuView)
    }
}
