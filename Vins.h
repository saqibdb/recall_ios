//
//  Vins.h
//  RecallMasters VIN Check
//
//  Created by ibuildx on 10/10/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "EmotionDatabase.h"


@interface Vins : EmotionDatabase

@property int VinId;
@property (strong,nonatomic) NSString *vehicleName;
@property (strong,nonatomic) NSString *VinNo;
@property  int recallCount;
@property (strong,nonatomic) NSString *time;


-(id) initWithName:(NSString*)_vehicleName andVinNo:(NSString*)_VinNo andRecallCounts:(int)_recallCount andTime:(NSString*)_time;

-(id) initWithId:(int)_VinId andName:(NSString*)_vehicleName andVinNo:(NSString*)_VinNo andRecallCount:(int)_recallCount andTime:(NSString*)_time ;



+(NSArray*)getAllVins;


-(BOOL) saveToDb ;
+(void)deleteVin : (int)vinId;

@end
